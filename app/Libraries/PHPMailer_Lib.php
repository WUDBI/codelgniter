<?php  namespace App\Libraries;

//defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter PHPMailer Class
 *
 * This class enables SMTP email with PHPMailer
 *
 * @category    Libraries
 * @author      CodexWorld
 * @link        https://www.codexworld.com
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class PHPMailer_Lib
{
 

    public function load(){
        // Include PHPMailer library files
        require_once APPPATH.'ThirdParty/PHPMailer/src/Exception.php';
        require_once APPPATH.'ThirdParty/PHPMailer/src/PHPMailer.php';
        require_once APPPATH.'ThirdParty/PHPMailer/src/SMTP.php';
        
        $mail = new PHPMailer;
        return $mail;
    }
}

?>