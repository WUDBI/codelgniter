<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

              <div class="container-fluid">
         
		 
		   <div class="row page-titles">
                 
                    <div class="col-md-7 align-self-left text-left">
                          <div class="d-flex">
                            <ol class="breadcrumb">
							<li class="breadcrumb-item">  <h4 class="text-themecolor">TIENDA:</h4></li>
                                <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_servicio') ?>">        LA BANCA  </a></li>
                                  <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_categoria') ?>">        Servicios web </a></li>
								  <li class="breadcrumb-item active">Páginas web</li>
                            </ol>
                            <!--<a class="btn btn-success d-none d-lg-block m-l-15" href="https://wrappixel.com/templates/elegant-admin/"> Upgrade To Pro</a>-->
                        </div>
                    </div>
                </div>
				
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card oh">
                            						
							
							  <div class="card-body" style=" background-color: #edf1f5;">
                                <div class="row text-center m-b-20">
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
									   <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">1000</h2><span class="text-muted">Página básica</span>
									  </a>
                                    </div>
									</div>
									</div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
									    <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">2700</h2><span class="text-muted">Hosting y dominio</span>
									  </a>
                                    </div>
									</div>
									</div>
									
									<div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
								  <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">300</h2><span class="text-muted">Página dinámica</span>
									  </a>
                                    </div>
									</div>
									</div>
									
								 <div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">

									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
								     <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">300</h2><span class="text-muted">Formularios bootstrap</span>
									  </a>
                                    </div>
									</div>
									</div>
									
								 <div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
								    <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">300</h2><span class="text-muted">Renovación de diseños web</span>
									  </a>
                                    </div>
									</div>
									</div>
									
								<div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_item') ?>" class="link">
								  <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="250" /><br><br>
                                        <h2 class="m-b-0 font-light">300</h2><span class="text-muted">Integración con redes sociales</span>
									  </a>
                                    </div>
									</div>
									</div>
                                
									
                                </div>
                            </div>
							 
								

                            <div class="card-body bg-light">
                                <div class="row text-center m-b-20">
                                  
								     <div class="col-lg-4 col-md-4 m-t-20">
									  <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 3'000.000</h2><span class="text-muted ">Total invertido</span>
									  </a>
                                    </div>
								    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">3500</h2><span class="text-muted ">Total # ventas</span>
									 </a>
                                    </div>
                                 
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 8'500.000</h2><span class="text-muted">Total vendido</span>
									 </a>
                                    </div>
									
                                </div>
                            </div>
							
							
							
							
                        </div>
                    </div>
             <div class="col-lg-4">
                                  <div class="card">
								    <div class="card-body">
									 <a href="#modal-item" data-toggle="modal" class="link">
										<center class="m-t-30"> <img src="<?= base_url() ?>/public/image/signo_mas.png" class="img-circle" width="30" />
											<h4 class="card-title m-t-10">Crea un nuevo producto o servicio</h4>
											<h6 class="card-subtitle">Configura tu producto o servicio</h6>
										 	<div class="col-sm-12">
														<button class="btn btn-alert">Configurar visor de la tienda</button>
										   </div>
										</center>
										</a>
									</div>
								 </div>
                           
				 <div id="modal-item" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>
								
								  <div class="card">
								    <div class="card-body">
									  <form class="form-horizontal form-material" enctype="multipart/form-data">
										<center class="m-t-30"> 
									
								          
											   <div class="form-group">
											   	
												<div class="col-sm-12">
													   <div class="form-group">
														<input type="text" placeholder="Johnathan Doe" value="Páginas web" class="form-control form-control-line card-title m-t-10">	
														</div>													
												</div>
									
											<br>
											
											
												 <div class="form-group">
												  <div class="col-md-12">
												      <textarea rows="5" class="form-control form-control-line">
													    salñasmdlas mflamflsamflasfmasñlfm slmañlfmasl fmlasmflasm flasmfasf fasfasfsaf salkndskfns nfksfnksdfkdskfsd kfdsfndsfndsfndsk fnsdklf
													 </textarea>
												  </div>
												</div>
												
												 <div class="form-group">
													<div class="col-sm-12">
														<button class="btn btn-success">Actualizar Clasificación</button>
													</div>
												</div>
											
											
										</center>
									 </form>
									</div>
								</div>    
                         
                             </div>
			</div>
             
         
            </div>
<?= $this->endSection() ?>