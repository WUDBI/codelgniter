<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

             <div class="container-fluid">
         
		 
		   <div class="row page-titles">
                 
                    <div class="col-md-7 align-self-left text-left">
                          <div class="d-flex">
                            <ol class="breadcrumb">
							<li class="breadcrumb-item">  <h4 class="text-themecolor">TIENDA:</h4></li>
                                <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_servicio') ?>">        LA BANCA  </a></li>
                                <li class="breadcrumb-item active">Servicios web</li>
                            </ol>
                            <!--<a class="btn btn-success d-none d-lg-block m-l-15" href="https://wrappixel.com/templates/elegant-admin/"> Upgrade To Pro</a>-->
                        </div>
                    </div>
                </div>
				
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card oh">
                            						
							
							  <div class="card-body" style=" background-color: #edf1f5;">
                                <div class="row text-center m-b-20">
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_clasificacion') ?>" class="link">
									 
                                        <h2 class="m-b-0 font-light">1000</h2><span class="text-muted">Páginas web</span>
									  </a>
                                    </div>
									</div>
									</div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_clasificacion') ?>" class="link">
									  
                                        <h2 class="m-b-0 font-light">2700</h2><span class="text-muted">Aplicaciones web</span>
									  </a>
                                    </div>
									</div>
									</div>
									
									<div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_clasificacion') ?>" class="link">
								
                                        <h2 class="m-b-0 font-light">300</h2><span class="text-muted">Configuración, imigración VPS</span>
									  </a>
                                    </div>
									</div>
									</div>
                                
									
                                </div>
                            </div>
							 
								

                            <div class="card-body bg-light">
                                <div class="row text-center m-b-20">
                                  
								     <div class="col-lg-4 col-md-4 m-t-20">
									  <a href="" class="link">
                                        <h2 class="m-b-0 font-light">$ 3'000.000</h2><span class="text-muted ">Total invertido</span>
									  </a>
                                    </div>
								    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="" class="link">
                                        <h2 class="m-b-0 font-light">3500</h2><span class="text-muted ">Total # ventas</span>
									 </a>
                                    </div>
                                 
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="" class="link">
                                        <h2 class="m-b-0 font-light">$ 8'500.000</h2><span class="text-muted">Total vendido</span>
									 </a>
                                    </div>
									
                                </div>
                            </div>
							
							
							
							
                        </div>
                    </div>
             <div class="col-lg-4">
                                  <div class="card">
								    <div class="card-body">
									 <a href="#modal-clasificacion" data-toggle="modal" class="link">
										<center class="m-t-30"> <img src="<?= base_url() ?>/public/image/signo_mas.png" class="img-circle" width="30" />
											<h4 class="card-title m-t-10">Crea una nueva clasificación</h4>
											<h6 class="card-subtitle">Crea productos o servicios para esta clasificación</h6>
										 	<div class="col-sm-12">
														<button class="btn btn-alert">Configurar visor de la tienda</button>
										   </div>
										</center>
										</a>
									</div>
								 </div>
								 
				<div id="modal-clasificacion" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>
                           
								
								  <div class="card">
								    <div class="card-body">
									  <form class="form-horizontal form-material" enctype="multipart/form-data">
										<center class="m-t-30"> 
										   <div class="sl-left"> <i class="fa fa-globe fa-3x text-info"></i></div>
								          
											   <div class="form-group">
											   	
												<div class="col-sm-12">
														<select class="form-control form-control-line">
															<option><h6 class="card-subtitle">Servicios web</h6></option>
															<option>Cosméticos naturales</option>
															<option>Vestuario</option>
															<option>Comida rápida</option>
															<option>Restaurante</option>
															<option>Multimedia</option>
															<option>Tecnología</option>
															<option>Plantas</option>
														</select>													
												</div>
									
											<br>
											
											
												 <div class="form-group">
												  <div class="col-md-12">
												      <textarea rows="5" class="form-control form-control-line">
													    salñasmdlas mflamflsamflasfmasñlfm slmañlfmasl fmlasmflasm flasmfasf fasfasfsaf salkndskfns nfksfnksdfkdskfsd kfdsfndsfndsfndsk fnsdklf
													 </textarea>
												  </div>
												</div>
												
												 <div class="form-group">
													<div class="col-sm-12">
														<button class="btn btn-success">Actualizar Categoría</button>
													</div>
												</div>
											
											
										</center>
									 </form>
									</div>
								</div>    
                         
                             </div>
			</div>
             
         
            </div>

<?= $this->endSection() ?>