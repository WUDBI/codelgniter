<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

           <div class="container-fluid">
         
	
				
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card oh">
                            <div class="card-body">
                              
							   <div class="row text-center m-b-10">
                                  
								  	
									 <div class="col-lg-3 col-md-3 m-t-10">
									   <a href="#modal-redes" data-toggle="modal" class="link">
                                       <img class="img-circle img-thumbnail" width="40" alt="redes" src="<?= base_url() ?>/public/image/redes/user.png"> 
									     <div class="font-small"><h2>1500</h2><i class="fa fa-cogs"></i></div>
									   </a>
                                    </div>
									
                                     <div class="col-lg-3 col-md-3 m-t-10">
									   <a href="#modal-redes" data-toggle="modal" class="link">
                                      <img class="img-circle img-thumbnail" width="40" alt="redes" src="<?= base_url() ?>/public/image/redes/facebook.png"> 
									    <div class="font-small"><h2>3200</h2><i class="fa fa-cogs"></i></div>
									   </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 m-t-10">
									  <a href="#modal-redes" data-toggle="modal" class="link">
                                         <img class="img-circle img-thumbnail" width="40" alt="redes" src="<?= base_url() ?>/public/image/redes/youtube.png"> 
										     <div class="font-small"><h2>2250</h2><i class="fa fa-cogs"></i></div>
									   </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 m-t-10">
									  <a href="#modal-redes" data-toggle="modal" class="link">
                                        <img class="img-circle img-thumbnail" width="40" alt="redes" src="<?= base_url() ?>/public/image/redes/instagram.png"> 
										<div class="font-small"><h2>5788</h2><i class="fa fa-cogs"></i></div>
									   </a>
                                    </div>
								
									
                                </div>
							  
                            </div>
							
				  <div id="modal-redes" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>
							 
								

                            <div class="card-body bg-light">
                                      
									
										
									
								<div class="row text-center m-b-20">
                                  
								  
								   <table id="dynamic-table" class="table">
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
															 Seleccionar
															</label>
														</th>					
														<th class="hidden-480">Nombre del cuento</th>
		                                  	            <th>Descripción</th>
														<th>Estado </th>
														<th>Precio</th>
														<th>Disponible</th>
														<th>ID</th>					
													</tr>
												</thead>
												<tbody>
													<tr>
													   <td>".$ASA."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$sd."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
	                                                <tr>
													   <td>".$sdsddd."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   	<tr>
													   <td>".$rere."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   	<tr>
													   <td>".$jkjhkhj."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   	<tr>
													   <td>".$ghghg."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   	<tr>
													   <td>".$aasas."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												   
												    	<tr>
													   <td>".$nmnmn."</td>
													   <td style='align-text:center'><h4>".$cuento_nombre."</h4><img src='".$img_ruta."' width='100' /></td>
													   <td><span style='width:10px;font-size:1.5em'>".$resumen."</span>...</td>
													   <td>".$estado."</td>
													   <td>".$cuento_precio."</td>
													   <td>".$estado_eliminado."</td>
													   <td>".$id_cuento."</td>
												   </tr>
												</tbody>
									</table>
                                </div>
                            </div>
							
							
							
							
                        </div>
                    </div>
      
             
         
            </div>
			
					<script src="<?= base_url() ?>/public/assets/js/jquery.dataTables.min.js"></script>
					
						
<script>
   $(document).ready(function(){  

  
  		//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					bAutoWidth: false,
					"aoColumns": [
					  { "bSortable": false },
					  null, null,null, null, null,
					  { "bSortable": false }
					],
					"aaSorting": [],
					
					
					//"bProcessing": true,
			        //"bServerSide": true,
			        //"sAjaxSource": "http://127.0.0.1/table.php"	,
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			
			
					select: {
						style: 'multi'
					}
			    } );
	
				
				
				
				

});

</script>


<?= $this->endSection() ?>

