<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

            <div class="container-fluid">
         
		 
		   <div class="row page-titles">
                 
                    <div class="col-md-7 align-self-left text-left">
                          <div class="d-flex">
                            <ol class="breadcrumb">
							<li class="breadcrumb-item">  <h4 class="text-themecolor">TIENDA:</h4></li>
                                <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_servicio') ?>">        LA BANCA  </a></li>
                                  <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_categoria') ?>">        Servicios web </a></li>
								   <li class="breadcrumb-item"><a href="<?= site_url('tienda/config_clasificacion') ?>">      Páginas web </a></li>
								  <li class="breadcrumb-item active">Página básica</li>
                            </ol>
                            <!--<a class="btn btn-success d-none d-lg-block m-l-15" href="https://wrappixel.com/templates/elegant-admin/"> Upgrade To Pro</a>-->
                        </div>
                    </div>
                </div>
				
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card oh">
                            						
							     <!-- Tab panes -->
                            <div class="card-body">
                                <form class="form-horizontal form-material">
                                    
									
									<div class="card-body bg-light">
									  <div class="form-group">
                                        <label class="col-md-12"> TITULO: </label>
                                        <div class="col-md-12">
                                             <input type="text" placeholder="" value="Característica  básicas" class="form-control form-control-line">
                                        </div>
                                      </div>
                                    

                                      <div class="form-group">
                                        <label class="col-md-12"> CAMPOS A MOSTRAR: </label>
                                        <div class="col-md-12">	
                                          <ul>										
										   <li> <input type="text" placeholder="" value="Adaptables a dispositivos móviles (responsive design)" class="form-control form-control-line">
                                      
                                           <li> <input type="text" placeholder="" value="Fáciles de navegar y de actualizar" class="form-control form-control-line" name="example-email" id="example-email">
                               
                                           <li> <input type="text" value="Seguros y protegidos de ataques cibernéticos" class="form-control form-control-line">
											<li><input type="text" value="Integrados con las redes sociales" class="form-control form-control-line">
										    </ul>
										</div>									
									  </div>
									</div>
							

		                           <div class="card-body bg-fondo">
									  <div class="form-group">
                                        <label class="col-md-12"> TITULO: </label>
                                        <div class="col-md-12">
                                             <input type="text" placeholder="" value="Digita tu cupón" class="form-control form-control-line">
											  DISMINUYE EL PRECIO EN: % <input type="text" placeholder="" value="0.05" class="form-control form-control-line">
                                        </div>
                                      </div>
                                    

                                      <div class="form-group">
                                        <label class="col-md-12"> CAMPOS A MOSTRAR: </label>
                                        <div class="col-md-12">										
										    <input type="text" disabled placeholder="" value="Campo de tipo texto reservado para el cliente" class="form-control form-control-line">
                                      
										</div>									
									  </div>
									</div>

									
									 <div class="card-body bg-light">
									  <div class="form-group">
                                        <label class="col-md-12"> TITULO: </label>
                                        <div class="col-md-12">
                                             <input type="text" placeholder="" value="Escribenos lo que deseas" class="form-control form-control-line">
										
                                        </div>
                                      </div>
                                    

                                      <div class="form-group">
                                        <label class="col-md-12"> CAMPOS A MOSTRAR: </label>
                                        <div class="col-md-12">										
										         <textarea rows="5" disabled placeholder="Campo de tipo descripción reservado para el cliente" class="form-control form-control-line"></textarea>
                                       
                                      
										</div>									
									  </div>
									</div>
									
									
									<div class="card-body bg-fondo">
									  <div class="form-group">
                                        <label class="col-md-12"> TITULO: </label>
                                        <div class="col-md-12">
                                             <input type="text" placeholder="" value="Agregar más características" class="form-control form-control-line">
                                        </div>
                                      </div>
                                    

                                      <div class="form-group">
                                        <label class="col-md-12"> CAMPOS A MOSTRAR: </label><br><br>
                                        <div class="col-md-12">	    <label class="col-md-12"> Campo de tipo selección reservado para el cliente </label><br><br>
                                          <div class="card-body bg-light">										
										    <input type="text" placeholder="" value="Sitios web 100% administrables" class="form-control form-control-line">
													  AUMENTA EL PRECIO EN: % <input type="text" placeholder="" value="0.05" class="form-control form-control-line">
                                         </div>
                                      
									    <div class="card-body bg-fondo2">	
                                            <input type="text" placeholder="" value="Enfocados en generar leads o conversiones" class="form-control form-control-line" name="example-email" id="example-email">
											
											  AUMENTA EL PRECIO EN: % <input type="text" placeholder="" value="0.08" class="form-control form-control-line">
                                        </div>
							   <div class="card-body bg-light">	
                                            <input type="text" value="Hosting y dominio" class="form-control form-control-line">
											  AUMENTA EL PRECIO EN: % <input type="text" placeholder="" value="0.2" class="form-control form-control-line">
						       </div>			  
							   <div class="card-body bg-fondo2">										
										<input type="text" value="Servidor VPS" class="form-control form-control-line">
											  AUMENTA EL PRECIO EN: % <input type="text" placeholder="" value="0.3" class="form-control form-control-line">
							<br>
							 CANTIDAD EN BODEGA: % <input type="text" placeholder="" value="100" class="form-control form-control-line">
							
							</div>
										
										</div>									
									  </div>
									</div>
                                    <br>
                                 
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success">Actualizar registros</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
							 
								

                            <div class="card-body bg-light">
                                <div class="row text-center m-b-20">
                                  
								     <div class="col-lg-4 col-md-4 m-t-20">
									  <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 3'000.000</h2><span class="text-muted ">Total invertido</span>
									  </a>
                                    </div>
								    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">3500</h2><span class="text-muted ">Total # ventas</span>
									 </a>
                                    </div>
                                 
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 8'500.000</h2><span class="text-muted">Total vendido</span>
									 </a>
                                    </div>
									
                                </div>
                            </div>
							
							
							
							
                        </div>
                    </div>
             <div class="col-lg-4">
                                                      
								   <div class="card">
								    <div class="card-body">
									
									 <a href="#modal-campo" data-toggle="modal" class="link">
										<center class="m-t-10"> <img src="<?= base_url() ?>/public/image/signo_mas.png" class="img-circle" width="30" />
											<h4 class="card-title m-t-10">Crea un nuevo campo de registro</h4>
											<h6 class="card-subtitle">Configura una característica</h6>
										 	<div class="col-sm-12">
														<button class="btn btn-alert">Configurar visor de la tienda</button>
										   </div>
										</center>
										</a>
									</div>
								 </div>
								 
				<div id="modal-campo" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>
								 
								  <div class="card">
								    <div class="card-body">
									  <form class="form-horizontal form-material" enctype="multipart/form-data">
										<center class="m-t-30"> 
												<div class="col-sm-12">
														<button class="btn btn-success">CREAR UNA PLANTILLA DE ESTE PRODUCTO</button>
												   </div><br>
									         <div class="form-group">
											   	
												<div class="col-sm-12">
													  
													       <label class="col-md-12"> PRECIO: </label>
														<input style='font-size:2em' type="text" placeholder="Johnathan Doe" value="$ 450000" class="form-control form-control-line card-title m-t-10">	
																									
												</div>
									         </div>
											 
											 
											 
								          <div class="form-group">
										      <label class="col-md-12"> IMAGEN PRÍNCIPAL: </label><br><br>
										   <img src="<?= base_url() ?>/public/image/photo2.png" class="img-thumbnail" width="350" /><br><br><br>
										     <div class="col-xs-3">
											  <input type="file" name="tienda_logo" />
											</div>
										</div>
										
										 <div class="form-group">
											   	
												<div class="col-sm-12">
													  
													       <label class="col-md-12"> LINK IMAGENES INSTAGRAM: </label>
														<input  type="text" placeholder="Johnathan Doe" value="https://www.instagram.com/p/B6BtUSrh97D/" class="form-control form-control-line card-title m-t-10">	
																									
												</div>
									         </div>
											 
											 	 <div class="form-group">
											   	
												<div class="col-sm-12">
													  
													       <label class="col-md-12"> LINK VIDEOS YOUTUBE: </label>
														<input  type="text" placeholder="Johnathan Doe" value="https://www.youtube.com/watch?v=2QcKN221g8I" class="form-control form-control-line card-title m-t-10">	
																									
												</div>
									         </div>
											 
											   <div class="form-group">
											   	
												<div class="col-sm-12">
													      <label class="col-md-12"> NOMBRE: </label>
														<input type="text" placeholder="Johnathan Doe" value="Página básica" class="form-control form-control-line card-title m-t-10">	
																							
												</div>
									         </div>
											
											
											
												 <div class="form-group">
												     <label class="col-md-12"> DESCRIPCIÓN: </label>
												  <div class="col-md-12">
												      <textarea rows="5" class="form-control form-control-line">
													    salñasmdlas mflamflsamflasfmasñlfm slmañlfmasl fmlasmflasm flasmfasf fasfasfsaf salkndskfns nfksfnksdfkdskfsd kfdsfndsfndsfndsk fnsdklf
													 </textarea>
												  </div>
												</div>
												
										<div class="form-group">
											   	
												<div class="col-sm-12">
													       <label class="col-md-12"> CANTIDAD EN BODEGA: </label>
														<input type="text" placeholder="Johnathan Doe" value="ILIMITADO" class="form-control form-control-line card-title m-t-10">	
																									
												</div>
									         </div>
											 
											 
												 <div class="form-group">
													<div class="col-sm-12">
														<button class="btn btn-success">Actualizar principal</button>
													</div>
												</div>
											
											
										</center>
									 </form>
									</div>
								</div>    
                         
                             </div>
			</div>
             
         
            </div>
<?= $this->endSection() ?>