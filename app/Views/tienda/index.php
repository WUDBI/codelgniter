<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">TIENDAS CREADAS</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                    
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
							 <a href="#modal-tienda" data-toggle="modal" class="link">
                                <center class="m-t-30"> <img src="<?= base_url() ?>/public/image/signo_mas.png" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10">Crea una nueva tienda</h4>
                                    <h6 class="card-subtitle">Personaliza tus ventas con tus redes sociales</h6>
                                 
                                </center>
							</a>
                            </div>
                        </div>
                    </div>
					
					<div id="modal-tienda" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>	
                    <!-- Column -->
                    <!-- Column -->
                 <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
							   <a href="<?= site_url('tienda/config_servicio') ?>" class="link">
                                <center class="m-t-30"> <img src="<?= base_url() ?>/public/assets/images/logo-icon.png" class="img-circle" width="150" />
                           
									<h4 class="card-title m-t-10">LA BANCA</h4>
                                    <h6 class="card-subtitle">Tienda de productos y/o servicios</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="<?= site_url('tienda/config_informe') ?>" class="link"><i class="icon-people"></i> <font class="font-medium">1100 <br> <span class="sl-date">compradores</span></font></a></div>
                                        <div class="col-4"><a href="<?= site_url('tienda/config_informe') ?>" class="link"><i class="icon-picture"></i> <font class="font-medium">5'800.000  <br><span class="sl-date"> ganancias</span></font></a></div>
                                    </div>
									
                                </center>
								</a>
                            </div>
                        </div>
                    </div>
					
					
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>


<?= $this->endSection() ?>