<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>

              <div class="container-fluid">
         
		 
		   <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">TIENDA: LA BANCA </h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                    
                        </div>
                    </div>
                </div>
				
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card oh">
                            <div class="card-body">
                              
							   <div class="row text-center m-b-20">
                                  
								  	
									 <div class="col-lg-3 col-md-3 m-t-20">
									   <a href="<?= site_url('tienda/config_redes') ?>" class="link">
                                       <img class="img-circle img-thumbnail" width="60" alt="redes" src="<?= base_url() ?>/public/image/redes/user.png"> 
									     <div class="font-medium"><h2>1500</h2>Compradores<span class="sl-date"> Hace 3 meses</span></div>
									   </a>
                                    </div>
									
                                     <div class="col-lg-3 col-md-3 m-t-20">
									   <a href="<?= site_url('tienda/config_redes') ?>" class="link">
                                      <img class="img-circle img-thumbnail" width="60" alt="redes" src="<?= base_url() ?>/public/image/redes/facebook.png"> 
									    <div class="font-medium"><h2>3200</h2>Fans<span class="sl-date"> En 3 meses</span></div>
									   </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 m-t-20">
									  <a href="<?= site_url('tienda/config_redes') ?>" class="link">
                                         <img class="img-circle img-thumbnail" width="60" alt="redes" src="<?= base_url() ?>/public/image/redes/youtube.png"> 
										     <div class="font-medium"><h2>2250</h2>Suscriptores<span class="sl-date"> En 2 meses </span></div>
									   </a>
                                    </div>
                                    <div class="col-lg-3 col-md-3 m-t-20">
									  <a href="<?= site_url('tienda/config_redes') ?>" class="link">
                                        <img class="img-circle img-thumbnail" width="60" alt="redes" src="<?= base_url() ?>/public/image/redes/instagram.png"> 
										<div class="font-medium"><h2>5788</h2>Seguidores<span class="sl-date"> En 1 mes</span></div>
									   </a>
                                    </div>
								
									
                                </div>
							  
                            </div>
							
							
							  <div class="card-body" style=" background-color: #edf1f5;">
                                <div class="row text-center m-b-20">
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_categoria') ?>" class="link">
									   <div class="sl-left"> <i class="fa fa-globe fa-3x text-info"></i></div>
                                        <h2 class="m-b-0 font-light">4000</h2><span class="text-muted">Servicio web y multimedia</span>
									  </a>
                                    </div>
									</div>
									</div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
									<div class="card">
								      <div class="card-body">
									  <a href="<?= site_url('tienda/config_categoria') ?>" class="link">
									    <div class="sl-left"> <i class="fa fa-leaf fa-3x text-primary"></i></div>
                                        <h2 class="m-b-0 font-light">6700</h2><span class="text-muted">Productos de cosméticos</span>
									  </a>
                                    </div>
									</div>
									</div>
                                
									
                                </div>
                            </div>
							 
								

                            <div class="card-body bg-light">
                                <div class="row text-center m-b-20">
                                  
								     <div class="col-lg-4 col-md-4 m-t-20">
									  <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 3'000.000</h2><span class="text-muted ">Total invertido</span>
									  </a>
                                    </div>
								    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">3500</h2><span class="text-muted ">Total # ventas</span>
									 </a>
                                    </div>
                                 
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="<?= site_url('tienda/config_informe') ?>" class="link">
                                        <h2 class="m-b-0 font-light">$ 8'500.000</h2><span class="text-muted">Total vendido</span>
									 </a>
                                    </div>
									
                                </div>
                            </div>
							
							
							
							
                        </div>
                    </div>
             <div class="col-lg-4">
                                  <div class="card">
								    <div class="card-body">
									 <a href="#modal-categoria" data-toggle="modal" class="link">
										<center class="m-t-30"> <img src="<?= base_url() ?>/public/image/signo_mas.png" class="img-circle" width="70" />
											<h4 class="card-title m-t-10">Crea una nueva categoría</h4>
											<h6 class="card-subtitle">Crea clasificaicones para esta categoría</h6>
										 	<div class="col-sm-12">
														<button class="btn btn-alert">Configurar visor de la tienda</button>
										   </div>
										</center>
										</a>
									</div>
								 </div>
								 
								 		
					<div id="modal-categoria" class="modal">
									<div class="modal-dialog">
										<div class="modal-content">
											<div id="modal-wizard-container">
												  <div class="card">
														<!-- Tab panes -->
														<div class="card-body">
														   <div id='x'>
														   Cargando...
														   </div>
												       </div>
                                                  </div>
											</div>
										</div>
									</div>
					</div>	
                           
								
								  <div class="card">
								    <div class="card-body">
									  <form class="form-horizontal form-material" enctype="multipart/form-data">
										<center class="m-t-30"> 
										<div class="form-group">
										   <img src="<?= base_url() ?>/public/assets/images/logo-icon.png" class="img-circle" width="150" /><br><br><br>
										     <div class="col-xs-3">
											  <input type="file" name="tienda_logo" />
											</div>
										</div>
								           <div class="form-group">
										    <input type="text" placeholder="Johnathan Doe" value="LA BANCA" class="form-control form-control-line card-title m-t-10">	
											</div>
											   <div class="form-group">
											   	
												<div class="col-sm-12">
														<select class="form-control form-control-line">
															<option><h6 class="card-subtitle">Tienda de productos y/o servicios</h6></option>
															<option>Tienda de productos</option>
															<option>Tienda de servicios</option>
														</select>													
												</div>
									
											<br>
											
											
												 <div class="form-group">
												  <div class="col-md-12">
												      <textarea rows="5" class="form-control form-control-line">
													    salñasmdlas mflamflsamflasfmasñlfm slmañlfmasl fmlasmflasm flasmfasf fasfasfsaf salkndskfns nfksfnksdfkdskfsd kfdsfndsfndsfndsk fnsdklf
													 </textarea>
												  </div>
												</div>
												
												 <div class="form-group">
													<div class="col-sm-12">
														<button class="btn btn-success">Actualizar Tienda</button>
													</div>
												</div>
											
											
										</center>
									 </form>
									</div>
								</div>    
                         
                             </div>
			</div>
             
         
            </div>

<?= $this->endSection() ?>