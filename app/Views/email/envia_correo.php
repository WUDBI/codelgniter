<?php
 
  // SMTP configuration
  $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
  $mail->isSMTP();                                            // Send using SMTP
  $mail->Host       = 'smtp.zoho.com';                    // Set the SMTP server to send through
  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
  $mail->Username   = 'admin@wudbi.com';                     // SMTP username
  $mail->Password   = '12345678_wudbi';                               // SMTP password
  $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
  $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

  
  $mail->setFrom('info@wudbi.com', 'CodexWorld');
  $mail->addReplyTo('info@wudbi.com', 'CodexWorld');
  
  // Add a recipient
  $mail->addAddress('jontherg707@gmail.com', 'Joe User');     // Add a recipient
  
  // Add cc or bcc 
  $mail->addCC('ventas@wudbi.com');
  $mail->addBCC('agenda@wudbi.com');
  
  // Email subject
  $mail->Subject = 'Send Email via SMTP using PHPMailer in CodeIgniter';
  
  // Set email format to HTML
  $mail->isHTML(true);
  
  $mail->addAttachment('<?= base_url() ?>/public/image/logo/logo_blanco.png', 'new.png');    // Optional name

  // Email body content
  $mailContent = "<h1>Send HTML Email using SMTP in CodeIgniter</h1>
      <p>This is a test email sending using SMTP mail server with PHPMailer.</p>";
  $mail->Body = $mailContent;
  
  // Send email
  if(!$mail->send()){
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
  }else{
      echo 'Message has been sent';
  }
?>