<?= $this->extend('templates/admin_templates') ?>

<?= $this->section('content') ?>


            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
         
                <!-- ============================================================== -->
                <!-- Yearly Sales -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-8">
                        <div class="card oh">
                            <div class="card-body">
                                <div class="d-flex m-b-30 align-items-center no-block">
                                    <h5 class="card-title ">Ventas de la tienda</h5>
                                    <div class="ml-auto">
                                        <ul class="list-inline font-12">
                                            <li><i class="fa fa-circle text-info"></i> Servicio web y multimedia</li>
                                            <li><i class="fa fa-circle text-primary"></i> Productos de cosméticos </li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="grafica_ventas" style="height: 350px;"></div>
                            </div>
                            <div class="card-body bg-light">
                                <div class="row text-center m-b-20">
								
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="informes.php" class="link">
                                        <h2 class="m-b-0 font-light">1500</h2><span class="text-muted">Total Ventas</span>
									</a>
                                    </div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="informes.php" class="link">
                                        <h2 class="m-b-0 font-light">700</h2><span class="text-muted">Servicio web y multimedia</span>
									</a>
                                    </div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
									 <a href="informes.php" class="link">
                                        <h2 class="m-b-0 font-light">800</h2><span class="text-muted">Productos de cosméticos</span>
									</a>
                                    </div>
									
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                 <h5 class="card-title">Tus seguidores</h5>
                                <h6 class="card-subtitle">Estas haciendo un buen trabajo...</h6>
                                <div class="steamline m-t-40">
								
								    <div class="sl-item">
                                        <div class="sl-left bg-success"> <i class="fa fa-user"></i></div>
                                        <div class="sl-right">
                                            <div class="font-medium"><h2>1500</h2>Compradores<span class="sl-date"> Hace 3 meses</span></div>
                                            <div class="desc"> <a href="javascript:void(0)" class="btn m-t-10 m-r-5 btn-rounded btn-outline-success">Revisar informe </a> </div>
                                        </div>
                                    </div>
                                    <div class="sl-item">
                                        <div class="sl-left "><img class="img-circle" alt="redes" src="<?= base_url() ?>/public/image/redes/facebook.png"> </div>
                                        <div class="sl-right">
                                            <div class="font-medium"><h2>3200</h2>Fans<span class="sl-date"> En 3 meses</span></div>
                                            <div class="desc">
											               <a href="javascript:void(0)" class="btn m-t-10 m-r-5 btn-rounded btn-outline-success">Revisar informe </a> </div>
											</div>
                                    </div>
                                   <div class="sl-item">
                                        <div class="sl-left "> <img class="img-circle" alt="redes" src="<?= base_url() ?>/public/image/redes/youtube.png"> </div>
                                        <div class="sl-right">
                                            <div class="font-medium"><h2>2250</h2>Suscriptores<span class="sl-date"> En 2 meses </span></div>
                                            <div class="desc">

											   <a href="javascript:void(0)" class="btn m-t-10 m-r-5 btn-rounded btn-outline-success">Revisar informe </a> </div>						
                            			  </div>
                                    </div>
                                    <div class="sl-item">
                                        <div class="sl-left"> <img class="img-circle" alt="redes" src="<?= base_url() ?>/public/image/redes/instagram.png"> </div>
                                        <div class="sl-right">
                                            <div class="font-medium"><h2>5788</h2>Seguidores<span class="sl-date"> En 1 mes</span></div>
                                            <div class="desc">
											             <a href="javascript:void(0)" class="btn m-t-10 m-r-5 btn-rounded btn-outline-success">Revisar informe </a> </div>
											</div>
                                    </div>
                                   
                                  
                                </div>
                            </div>
                        </div>
                    </div>
			</div>
             
         
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
   


<script>
   $(document).ready(function(){  
// Use Morris.Area instead of Morris.Line

var data = [
      { y: '2020-07-01', a: 300000, b: 500000},
      { y: '2020-07-02', a: 200000,  b: 600000},
	  { y: '2020-07-03', a: 350000, b: 520000},
      { y: '2020-07-04', a: 200000,  b: 600000},
	  { y: '2020-07-05', a: 200000,  b: 600000},
	  { y: '2020-07-06', a: 200000,  b: 600000},
	  { y: '2020-07-07', a: 300000, b: 500000},
      { y: '2020-07-08', a: 100000,  b: 800000},
	  { y: '2020-07-09', a: 800000,  b: 200000},
	  { y: '2020-07-10', a: 200000,  b: 600000},
	  { y: '2020-07-11', a: 300000, b: 500000},
      { y: '2020-07-12', a: 900000,  b: 600000},
	  { y: '2020-07-13', a: 200000,  b: 900000},
	  { y: '2020-07-14', a: 300000, b: 500000},
      { y: '2020-07-15', a: 200000,  b: 600000},
	  { y: '2020-07-16', a: 300000, b: 500000},
      { y: '2020-07-17', a: 200000,  b: 660000},
	  { y: '2020-07-18', a: 200000,  b: 600000},
	  { y: '2020-07-19', a: 200000,  b: 800000},
	  { y: '2020-07-20', a: 400000, b: 500000},
      { y: '2020-07-21', a: 200000,  b: 600000},
	  { y: '2020-07-22', a: 1000000,  b: 600000},
	  { y: '2020-07-23', a: 200000,  b: 600000},
	  { y: '2020-07-24', a: 330000, b: 500000},
      { y: '2020-07-25', a: 200000,  b: 600000},
	  { y: '2020-07-26', a: 200000,  b: 650000},
	  { y: '2020-07-27', a: 500000,  b: 400000},
	  { y: '2020-07-28', a: 200000,  b: 600000},
	  { y: '2020-07-29', a: 800000,  b: 600000},
	  { y: '2020-07-30', a: 300000, b: 500000},
      { y: '2020-07-31', a: 200000,  b: 600000}

    ],
    config = {
      data: data,
      xkey: 'y',
      ykeys: ['a', 'b'],
	  xLabels: 'day',
 // xLabelAngle: 45,
  xLabelFormat: function (d) {
  
/*  var weekdays = new Array(7);
    weekdays[0] = "DOMINGO";
    weekdays[1] = "LUNES";
    weekdays[2] = "MARTES";
    weekdays[3] = "MIERCOLES";
    weekdays[4] = "JUEVES";
    weekdays[5] = "VIERNES";
    weekdays[6] = "SÁBADO";

    return weekdays[d.getDay()] + '-' + (d.getDate());
	*/
	return d.getDate();
  }, 
  
   preUnits: "$",
  smooth: true,
  resize: true,
      labels: ['Total web', 'Total cosméticos'],
      fillOpacity: 0.6,
      hideHover: 'auto',
      behaveLikeLine: true,
      resize: true,
      pointFillColors:['#ffffff'],
      pointStrokeColors: ['black'],
      lineColors:['#009efb','#7460ee']
  };
  config.element = 'grafica_ventas';
Morris.Area(config);
config.element = 'grafica_cursos';
Morris.Line(config);
config.element = 'grafica_voluntarios';
Morris.Bar(config);
config.element = 'grafica_eventos';
config.stacked = true;
Morris.Bar(config);
Morris.Donut({
  element: 'grafica_general',
  data: [
    {label: "Friends", value: 30},
    {label: "Allies", value: 15},
    {label: "Enemies", value: 45},
    {label: "Neutral", value: 10}
  ]
  
});

});

</script>
<?= $this->endSection() ?>