<!DOCTYPE html>

<html class="no-js" lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>WUDBI WEB</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="App Landing &raquo; Feed" href="https://demo.kaliumtheme.com/landing/feed/" />
<link rel="alternate" type="application/rss+xml" title="App Landing &raquo; Comments Feed" href="https://demo.kaliumtheme.com/landing/comments/feed/" />
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141030632-1"></script>
<script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-141030632-1', {"groups":"laborator_analytics","link_attribution":true,"linker":{"accept_incoming":true,"domains":["laborator.co","kaliumtheme.com","oxygentheme.com","neontheme.com","themeforest.net","laborator.ticksy.com"]}});
gtag('config', 'AW-991533214');</script>    <meta property="og:type" content="article">
    <meta property="og:title" content="Homepage">
    <meta property="og:url" content="https://demo.kaliumtheme.com/landing/">
    <meta property="og:site_name" content="App Landing">

	
			<script>
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo.kaliumtheme.com\/landing\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.2"}};
			/*! This file is auto-generated */
			!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style>
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>

<link rel='stylesheet' id=''  href='<?= base_url() ?>/public/assets/css/style.css' media='all' />



	<link rel='stylesheet' id='wp-block-library-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-includes/css/dist/block-library/style.min.css?ver=5.4.2' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/css/rs6.css?ver=6.2.12' media='all' />
<style id='rs-plugin-settings-inline-css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='typolab-a62b0b43c590dff2f54c9dca53eb0672-css'  href='https://fonts.googleapis.com/css?family=NTR%3Aregular&#038;subset=latin&#038;display=swap&#038;ver=35330e1e41d37d3b171d8ca07983d7ed' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=6.2.0' media='all' />
<link rel='stylesheet' id='kalium-bootstrap-css-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/css/bootstrap.min.css?ver=3.0.5.001' media='all' />
<link rel='stylesheet' id='kalium-theme-base-css-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/css/base.min.css?ver=3.0.5.001' media='all' />
<link rel='stylesheet' id='kalium-theme-other-css-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/css/other.min.css?ver=3.0.5.001' media='all' />
<link rel='stylesheet' id='kalium-style-css-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/style.css?ver=3.0.5.001' media='all' />
<link rel='stylesheet' id='custom-skin-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium-child-landing/custom-skin.css?ver=7d0bb1d50c2a62dac55386dec41cc4df' media='all' />
<link rel='stylesheet' id='kalium-child-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium-child-landing/style.css?ver=5.4.2' media='all' />
    <script type="text/javascript">
		var ajaxurl = ajaxurl || 'https://demo.kaliumtheme.com/landing/wp-admin/admin-ajax.php';
		    </script>
	<style id="typolab-font-variants">body,p{font-family:'NTR';font-weight:400}.button{font-family:'NTR';font-weight:400}.main-header.menu-type-standard-menu .standard-menu-container div.menu>ul>li>a,.main-header.menu-type-standard-menu .standard-menu-container ul.menu>li>a{font-family:'NTR';font-weight:400}.feature-block h2{font-family:'NTR';font-weight:400}.feature-block p{font-family:'NTR';font-weight:400}.weather-icons h2{font-family:'NTR';font-weight:400}.title h2{font-family:'NTR';font-weight:400}.title p{font-family:'NTR';font-weight:400}.weather-icons p{font-family:'NTR';font-weight:400}#portafolio .title h2{font-family:'NTR';font-weight:400}#portafolio .title p{font-family:'NTR';font-weight:400}#contacto h2{font-family:'NTR';font-weight:400}#footer p{font-family:'NTR';font-weight:400}.post-formatting .wp-caption .wp-caption-text{font-family:'NTR';font-weight:400}.widget.widget_product_search .search-bar input[name="s"],.widget.widget_search .search-bar input[name="s"]{font-family:'NTR';font-weight:400}body,p{font-size:20px}.main-header.menu-type-standard-menu .standard-menu-container div.menu>ul>li>a,.main-header.menu-type-standard-menu .standard-menu-container ul.menu>li>a{font-size:20px}.feature-block h2{font-size:30px}.feature-block p{font-size:20px}.weather-icons h2{font-size:54px}.title h2{font-size:64px}.title p{font-size:30px}.weather-icons p{font-size:29px}#portafolio .title h2{font-size:52px}#portafolio .title p{font-size:21px}#contacto h2{font-size:35px}#footer p{font-size:18px}.post-formatting .wp-caption .wp-caption-text{font-size:16px}.widget.widget_product_search .search-bar input[name="s"],.widget.widget_search .search-bar input[name="s"]{font-size:18px}</style><script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/js/rbtools.min.js?ver=6.0.7'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.2.12'></script>
<!--
<link rel='https://api.w.org/' href='https://demo.kaliumtheme.com/landing/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demo.kaliumtheme.com/landing/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.4.2" />
<link rel="canonical" href="https://demo.kaliumtheme.com/landing/" />
<link rel='shortlink' href='https://demo.kaliumtheme.com/landing/' />
<link rel="alternate" type="application/json+oembed" href="https://demo.kaliumtheme.com/landing/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.kaliumtheme.com%2Flanding%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://demo.kaliumtheme.com/landing/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemo.kaliumtheme.com%2Flanding%2F&#038;format=xml" />
 -->
    <!-- Facebook Pixel Code -->
    <script>
		!function ( f, b, e, v, n, t, s ) {
			if ( f.fbq ) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply( n, arguments ) : n.queue.push( arguments )
			};
			if ( !f._fbq ) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement( e );
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName( e )[ 0 ];
			s.parentNode.insertBefore( t, s )
		}( window,
			document, 'script', 'https://connect.facebook.net/en_US/fbevents.js' );
		fbq( 'init', '261506237612914' ); // Insert your pixel ID here.
		fbq( 'track', 'PageView' );

		fbq( 'track', 'ViewContent', {
			id: 'landing',
			title: 'App Landing - Demo Content',
			categories: ['demo-content'],
		} );
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=261506237612914&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
	<style data-appended-custom-css="true">.site-header {
position: absolute;
 left: 0;
 right: 0;

}</style><meta name="theme-color" content="#f9c81e">		            <link rel="shortcut icon" href="<?= base_url() ?>/public/image/logo/favicon.png">
						<script>var mobile_menu_breakpoint = 768;</script><style data-appended-custom-css="true">@media screen and (min-width:769px) { .mobile-menu-wrapper,.mobile-menu-overlay,.header-block__item--mobile-menu-toggle {
display: none;

} }</style><style data-appended-custom-css="true">@media screen and (max-width:768px) { .header-block__item--standard-menu-container {
display: none;

} }</style><meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<meta name="generator" content="Powered by Slider Revolution 6.2.12 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){
			//window.requestAnimationFrame(function() {				 
				window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
				window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
				try {								
					var pw = document.getElementById(e.c).parentNode.offsetWidth,
						newh;
					pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
					e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
					e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
					e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
					e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
					e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
					e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
					e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
					if(e.layout==="fullscreen" || e.l==="fullscreen") 						
						newh = Math.max(e.mh,window.RSIH);					
					else{					
						e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
						for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
						e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
						e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
						for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
											
						var nl = new Array(e.rl.length),
							ix = 0,						
							sl;					
						e.tabw = e.tabhide>=pw ? 0 : e.tabw;
						e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
						e.tabh = e.tabhide>=pw ? 0 : e.tabh;
						e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
						for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
						sl = nl[0];									
						for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
						var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
						newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
					}				
					if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
					document.getElementById(e.c).height = newh+"px";
					window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
				} catch(e){
					console.log("Failure at Presize of Slider:" + e)
				}					   
			//});
		  };</script>
<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1463148379933{border-bottom-width: 1px !important;border-bottom-color: #dddddd !important;border-bottom-style: solid !important;}.vc_custom_1463148754510{border-bottom-width: 1px !important;background-color: #fafafa !important;border-bottom-color: #dddddd !important;border-bottom-style: solid !important;}.vc_custom_1464268532742{padding-top: 40px !important;}.vc_custom_1463492522689{background-image: url(https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/bg-1.png?id=124) !important;background-position: 0 0 !important;background-repeat: repeat !important;}.vc_custom_1498220066719{padding-top: 0px !important;}.vc_custom_1498224592367{padding-top: 0px !important;}.vc_custom_1498219725582{padding-top: 0px !important;}.vc_custom_1485528964151{margin-bottom: 0px !important;}.vc_custom_1464261818858{margin-bottom: 0px !important;}.vc_custom_1464261826530{margin-bottom: 0px !important;}.vc_custom_1464261833695{margin-bottom: 0px !important;}.vc_custom_1464261844756{margin-bottom: 0px !important;}.vc_custom_1464261852421{margin-bottom: 0px !important;}.vc_custom_1463053160569{margin-bottom: 0px !important;}.vc_custom_1463052357364{padding-top: 40px !important;}.vc_custom_1498224749148{margin-right: 0px !important;margin-left: 0px !important;padding-top: 35px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1498224614541{margin-right: 0px !important;margin-left: 0px !important;padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1498224617960{margin-right: 0px !important;margin-left: 0px !important;padding-top: 0px !important;padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1464261676761{margin-bottom: 0px !important;}.vc_custom_1464261716529{margin-bottom: 0px !important;}.vc_custom_1464261725872{margin-bottom: 0px !important;}.vc_custom_1464261732658{margin-bottom: 0px !important;}.vc_custom_1464261739831{margin-bottom: 0px !important;}.vc_custom_1464261747536{margin-bottom: 0px !important;}.vc_custom_1463055785974{margin-top: 0px !important;padding-top: 0px !important;padding-bottom: 20px !important;}.vc_custom_1464268545885{margin-bottom: 30px !important;padding-top: 30px !important;}.vc_custom_1498219675912{padding-top: 0px !important;}.vc_custom_1498219678994{padding-top: 0px !important;}.vc_custom_1463059209186{margin-bottom: 0px !important;padding-top: 50px !important;}</style><noscript><style> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>

<body class="home page-template-default page page-id-2 header-absolute has-fixed-footer wpb-js-composer js-comp-ver-6.2.0 vc_responsive">

<!-- MENU DISPOSITIVO MOBILE AQUI-->
<div class="mobile-menu-wrapper mobile-menu-slide">

    <div class="mobile-menu-container">

			<ul id="menu-one-page-menu" class="menu"><li id="menu-item-225" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-225"><a href="#overview">Nosotros</a></li>
				<li id="menu-item-226" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-226"><a href="#features">Servicios web</a></li>
				<li id="menu-item-227" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-227"><a href="#portafolio">Portafolio</a></li>
				<li id="menu-item-230" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-230"><a href="#ofertas">Oferta</a></li>

				<li id="menu-item-229" class="get-the-app menu-item menu-item-type-custom menu-item-object-custom menu-item-229"><a href="#contacto">Contáctanos</a></li>
			</ul>
		          <!--  <form role="search" method="get" class="search-form" action="https://demo.kaliumtheme.com/landing/">
                <input type="search" class="search-field" placeholder="Search site..." value="" name="s" id="search_mobile_inp"/>

                <label for="search_mobile_inp">
                    <i class="fa fa-search"></i>
                </label>

                <input type="submit" class="search-submit" value="Go"/>
            </form>-->	
    </div>

</div>
 <!-- FIN MENU DISPOSITIVO MOBILE -->


<!-- MENU PRINCIPAL WEB AQUI -->

<div class="mobile-menu-overlay"></div>
<div class="wrapper" id="main-wrapper">

		<style data-appended-custom-css="true">.site-header {
			box-shadow: 0px 0px 0px transparent
			}
		</style>  

  <header class="site-header main-header menu-type-standard-menu is-sticky">

   <div class="header-block">

	
	<div class="header-block__row-container container">

		<div class="header-block__row header-block__row--main">
		
     <!-- LOGO AQUI -->
      <div class="header-block__column header-block__logo header-block--auto-grow">
			<style data-appended-custom-css="true">
						.logo-image {
							width:159px;


							}
							.btn_accion{

							}
							.btn_accion a{
                             background:#ff9300;
							 color:#fff;
							 padding:5px 10px;
							 border-radius:6px;
							}

							.btn_accion2 a{
                             background:#fff;
							 color:#ff9300;
							 padding:5px 10px;
							 border-radius:6px;
							}
							.btn_accion3 a{
							padding:15px;
							}
							.btn_accion3 a{
                             background:#f9f6f1;
							 color:#c69881;
							 padding:5px 10px;
							 border-radius:6px;
							
							}
							.btn_accion3 a:hover{
								background:#c69881;
								color:#fff;
							}
							/*c69881 */
							.titulos{
								/*background:#f9f6f1;*/
								padding:10px;
								border-radius:6px;
								color:#b5b2ad;
							}
						
							#corte_img img{
								width: 300px;
								height: 400px;
							    
								}
								@supports(object-fit: cover){
									#corte_img img{
									height: 400px;
									object-fit: cover;
									object-position: top center;
									}
								}
							@media (max-width:768px){
								.logo-image {
							width:90px;


							}
							}
			</style>
			<a href="#" class="header-logo logo-image">
				<img src="<?= base_url() ?>/public/image/logo/logo_blanco.png" class="main-logo" width="140"  alt="wudbi - Landing Page"/>
			</a>
       </div>
	 <!--FIN LOGO  -->

   <!-- MENU AQUI -->
	    <div class="header-block__column header-block--content-right header-block--align-right">

            <div class="header-block__items-row">
				<div class="header-block__item header-block__item--type-menu-main-menu header-block__item--standard-menu-container">
				<div class="standard-menu-container menu-skin-light">
				<nav class="nav-container-main-menu">
				<ul id="menu-one-page-menu-1" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-225"><a href="#overview"><span>Nosotros</span></a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-226"><a href="#features"><span>Servicios</span></a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-227"><a href="#portafolio"><span>Portafolio</span></a></li>
				<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-230"><a href="#ofertas"><span>Ofertas</span></a></li>

				<li class="get-the-app menu-item menu-item-type-custom menu-item-object-custom menu-item-229"><a href="#contacto"><span>Contáctanos</span></a></li>
				</ul>
	            </nav>
	           </div>
               </div>

				<!-- ICONO MENU MOBILE AQUI -->
				<div class="header-block__item header-block__item--type-menu-main-menu header-block__item--mobile-menu-toggle">
				<a href="#" class="toggle-bars menu-skin-light" data-action="mobile-menu">        <span class="toggle-bars__column">
							<span class="toggle-bars__bar-lines">
								<span class="toggle-bars__bar-line toggle-bars__bar-line--top"></span>
								<span class="toggle-bars__bar-line toggle-bars__bar-line--middle"></span>
								<span class="toggle-bars__bar-line toggle-bars__bar-line--bottom"></span>
							</span>
						</span>
						</a>
				</div>           

            </div>

        </div>
	</div>

</div>

	
</div>

</header>

<!--FIN MENU GENERAL  AQUI -->




<!----PORTADA AQUI ---->
    <div class="vc-container">
		<div class="vc-row-container container"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">
		
		
		
		
			<!-- START Landing Page REVOLUTION SLIDER 6.2.12 -->
		
			<p class="rs-p-wp-fix"></p>
			<rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
				<rs-module id="rev_slider_1_1" style="" data-version="6.2.12">
					<rs-slides>
						<rs-slide data-key="rs-3" data-title="DESARROLLO WEB" data-thumb="<?= base_url() ?>/public/image/portada/portada4.png" data-anim="ei:d;eo:d;s:600;r:0;t:fade;sl:d;">
							<img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" alt="Weathy Forecast" title="Homepage" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/transparent.png" data-bg="c:#ff9201;" data-parallax="off" class="rev-slidebg" data-no-retina>
<!--
							--><rs-layer
								id="slider-1-slide-3-layer-2" 
								class="rs-pxl-2"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:1232px;y:71px;"
								data-text="l:22;"
								data-dim="w:115px;h:117px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:5;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="230" height="233" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/sun.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-9" 
								class="rs-pxl-4"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:1117px;y:116px;"
								data-text="l:22;"
								data-dim="w:262px;h:146px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:6;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="298" height="165" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/cloud1.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-10" 
								class="rs-pxl-7"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:1005px;y:152px;"
								data-text="l:22;"
								data-dim="w:149px;h:83px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:7;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="524" height="291" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/cloud2.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-4" 
								class="rs-pxl-1"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:-500px;y:611px;"
								data-text="l:22;"
								data-dim="w:2503px;h:354px;"
								data-frame_0="sX:0.9;sY:0.9;tp:600;"
								data-frame_1="tp:600;st:500;sp:1500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:7000;"
								style="z-index:8;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="4998" height="707" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/wave.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-8" 
								class="rs-pxl-1"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:255px;y:327px;"
								data-text="l:22;"
								data-dim="w:188px;h:188px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:9;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="376" height="376" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/ray.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-3" 
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:786px;y:270px;"
								data-text="l:22;"
								data-dim="w:554px"
								data-frame_0="x:50px;tp:600;"
								data-frame_1="tp:600;e:power1.out;st:800;sR:800;"
								data-frame_999="x:50px;o:0;tp:600;e:nothing;st:w;sR:7900;"
								style="z-index:10;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="708" height="1231" data-lazyload="<?= base_url() ?>/public/image/portada/portada4.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-5" 
								data-type="text"
								data-color="rgba(255, 255, 255, 1)"
								data-rsp_ch="on"
								data-xy="x:80px;y:120px;"
								data-text="s:125;l:185;"
								data-frame_0="o:1;tp:600;"
								data-frame_0_chars="y:-50px;o:0;"
								data-frame_1="tp:600;e:power4.inOut;st:40;sp:1000;sR:40;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:7610;"
								style="z-index:11;font-family:'function_promedium', font-size:1em, 'NTR', sans-serif;;"
							>¿Quieres emprender?
							</rs-layer><!--
							
							</rs-layer><!--
							--><rs-layer
								id="slider-1-slide-3-layer-6" 
								data-type="text"
								data-color="rgba(255, 255, 255, 1)"
								data-rsp_ch="on"
								data-xy="x:80px;y:305px;"
								data-text="w:normal;s:44;l:50;"
								data-dim="w:542px;"
								data-frame_0="y:100%;tp:600;"
								data-frame_0_mask="u:t;y:100%;"
								data-frame_1="tp:600;e:power2.inOut;st:770;sp:2000;sR:770;"
								data-frame_1_mask="u:t;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:6230;"
								style="z-index:12;font-family:'function_promedium', 'NTR', sans-serif;;"
							>WUDBI desarrolla y crea tu presencia en la web.
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-3-layer-7" 
								data-type="text"
								data-color="rgba(255, 255, 255, 1)"
								data-rsp_ch="on"
								data-xy="x:80px;y:452px;"
								data-text="s:25;l:25;"
								data-frame_0="y:100%;tp:600;"
								data-frame_0_mask="u:t;y:100%;"
								data-frame_1="tp:600;e:power2.inOut;st:1010;sp:2000;sR:1010;"
								data-frame_1_mask="u:t;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:5990;"
								style="z-index:13;font-family:'function_promedium', 'NTR', sans-serif;;"
							>— <a href="#contacto"> Registrate  y conoce todo lo que podemos ofrecer para ti!</a> 
							</rs-layer><!--
-->						</rs-slide>
						<rs-slide data-key="rs-1" data-title="NEGOCIOS DIGITALES" data-thumb="<?= base_url() ?>/public/image/portada/portada1.png" data-anim="ei:d;eo:d;s:600;r:0;t:fade;sl:d;">
							<img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" alt="Sunny or Rainy" title="Homepage" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/transparent.png" data-bg="c:#1b67d4;" data-parallax="off" class="rev-slidebg" data-no-retina>
<!--
							--><rs-layer
								id="slider-1-slide-1-layer-13" 
								class="rs-pxl-2"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:924px;y:183px;"
								data-text="l:22;"
								data-dim="w:170px;h:95px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:1710;sR:1710;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:6990;"
								style="z-index:5;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="340" height="190" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/blue_cloud.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-1-layer-12" 
								class="rs-pxl-3"
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:993px;y:174px;"
								data-text="l:22;"
								data-dim="w:262px;h:146px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:1330;sR:1330;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:7370;"
								style="z-index:6;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="524" height="291" data-lazyload="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/blue_cloud2.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-1-layer-6" 
								data-type="text"
								data-color="rgba(255, 255, 255, 1)"
								data-rsp_ch="on"
								data-xy="x:466px;y:280px;"
								data-text="w:normal;s:44;l:50;"
								data-dim="w:633px;"
								data-frame_0="sX:0.9;sY:0.9;tp:600;"
								data-frame_1="tp:600;st:710;sp:1500;sR:710;"
								data-frame_999="st:w;sp:500;auto:true;"
								data-frame_999_mask="u:t;"
								style="z-index:7;font-family:'function_promedium', 'NTR', sans-serif;;"
							>WUDBI es tu puente hacia el futuro, crearemos una estrategia a la medida para tu proyecto. 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-1-layer-7" 
								data-type="text"
								data-color="rgba(255, 255, 255, 1)"
								data-rsp_ch="on"
								data-xy="x:465px;y:520px;"
								data-text="s:25;l:25;"
								data-frame_0="o:1;tp:600;"
								data-frame_0_chars="y:-100%;o:1;rZ:35deg;"
								data-frame_0_mask="u:t;"
								data-frame_1="tp:600;e:power4.inOut;st:1140;sp:1080;sR:1140;"
								data-frame_1_mask="u:t;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:6080;"
								style="z-index:8;font-family:'function_promedium', 'NTR', sans-serif;;"
							>- Si eres emprendedor, <a href="#contacto" style="color:#b48484" />ingresa aquí</a>. 
							</rs-layer><!--

							--><rs-layer
								id="slider-1-slide-1-layer-11" 
								data-type="image"
								data-rsp_ch="on"r
								data-xy="x:-69px;y:147px;"
								data-text="l:22;"
								data-dim="w:479px;h:655px;"
								data-frame_0="y:50px;tp:600;"
								data-frame_1="tp:600;e:power3.out;st:500;sR:500;"
								data-frame_999="y:50px;o:0;tp:600;e:power1.in;st:w;sR:8200;"
								style="z-index:9;"
							><img src="<?= base_url() ?>/public/image/portada/portada1.png" width="959" height="1310" data-lazyload="<?= base_url() ?>/public/image/portada/portada1.png" data-no-retina> 
							</rs-layer><!--
-->						</rs-slide>
					</rs-slides>
				</rs-module>
				<script type="text/javascript">
					setREVStartSize({c: 'rev_slider_1_1',rl:[1240,1024,768,480],el:[],gw:[1170],gh:[800],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
					var	revapi1,
						tpj;
					jQuery(function() {
						tpj = jQuery;
						revapi1 = tpj("#rev_slider_1_1")
						if(revapi1==undefined || revapi1.revolution == undefined){
							revslider_showDoubleJqueryError("rev_slider_1_1");
						}else{
							revapi1.revolution({
								sliderLayout:"fullwidth",
								visibilityLevels:"1240,1024,768,480",
								gridwidth:1170,
								gridheight:800,
								lazyType:"smart",
								spinner:"spinner3",
								perspectiveType:"local",
								responsiveLevels:"1240,1024,768,480",
								shuffle:true,
								progressBar:{disableProgressBar:true},
								navigation: {
									keyboardNavigation:true,
									mouseScrollNavigation:false,
									onHoverStop:false,
									touch: {
										touchenabled:true,
										swipe_min_touches:50
									},
									bullets: {
										enable:true,
										tmp:"<span class=\"tp-bullet-image\"></span><span class=\"tp-bullet-imageoverlay\"></span><span class=\"tp-bullet-title\">{{title}}</span>",
										style:"zeus",
										hide_onmobile:true,
										hide_under:600,
										hide_onleave:true,
										h_align:"right",
										v_align:"center",
										h_offset:30,
										v_offset:30,
										direction:"vertical"
									}
								},
								parallax: {
									levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
									type:"mouse",
									origo:"slidercenter",
									speed:2000
								},
								fallbacks: {
									allowHTML5AutoPlayOnAndroid:true
								},
							});
						}
						
					});
				</script>
				<script>
					var htmlDivCss = '	#rev_slider_1_1_wrapper rs-loader.spinner3 div { background-color: #FFFFFF !important; } ';
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				</script>
				<script>
					var htmlDivCss = unescape("%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%20%7B%0A%20%20%20%20%20box-sizing%3Acontent-box%3B%20-webkit-box-sizing%3Acontent-box%3B%20border-radius%3A50%25%3B%0A%20%20%20%20%20%20background-color%3A%20rgba%280%2C%200%2C%200%2C%200%29%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20opacity%200.3s%20ease%3B%0A%20%20%20%20%20%20transition%3A%20opacity%200.3s%20ease%3B%0A%20%20%20%20width%3A13px%3Bheight%3A13px%3B%0A%20%20%20%20border%3A2px%20solid%20%23ffffff%3B%0A%20%7D%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%3Aafter%20%7B%0A%20%20content%3A%20%27%27%3B%0A%20%20position%3A%20absolute%3B%0A%20%20width%3A%20100%25%3B%0A%20%20height%3A%20100%25%3B%0A%20%20left%3A%200%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background-color%3A%20%23ffffff%3B%0A%20%20-webkit-transform%3A%20scale%280%29%3B%0A%20%20transform%3A%20scale%280%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%2050%25%3B%0A%20%20transform-origin%3A%2050%25%2050%25%3B%0A%20%20-webkit-transition%3A%20-webkit-transform%200.3s%20ease%3B%0A%20%20transition%3A%20transform%200.3s%20ease%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%3Ahover%3Aafter%2C%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet.selected%3Aafter%7B%0A%20%20%20%20-webkit-transform%3A%20scale%281.2%29%3B%0A%20%20transform%3A%20scale%281.2%29%3B%0A%7D%0A%20%20%0A%20%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet-image%2C%0A%20%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet-imageoverlay%7B%0A%20%20%20%20%20%20%20%20width%3A135px%3B%0A%20%20%20%20%20%20%20%20height%3A60px%3B%0A%20%20%20%20%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20%20%20%20%20background%3A%23000%3B%0A%20%20%20%20%20%20%20%20background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20%20%20%20%20%20%20bottom%3A13px%3B%0A%20%20%20%20%20%20%20%20margin-bottom%3A10px%3B%0A%20%20%20%20%20%20%20%20transform%3AtranslateX%28-50%25%29%3B%0A%20%20%20%20%20%20%20-webkit-transform%3AtranslateX%28-50%25%29%3B%0A%20%20%20%20%20%20%20%20box-sizing%3Aborder-box%3B%0A%20%20%20%20%20%20%20%20background-size%3Acover%3B%0A%20%20%20%20%20%20%20%20background-position%3Acenter%20center%3B%0A%20%20%20%20%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20%20%20opacity%3A0%3B%0A%20%20%20%20%20%20%20%20%20-webkit-backface-visibility%3A%20hidden%3B%20%0A%20%20%20%20%20%20%20%20backface-visibility%3A%20hidden%3B%0A%20%20%20%20%20%20%20%20-webkit-transform-origin%3A%2050%25%2050%25%3B%0A%20%20%20%20transform-origin%3A%2050%25%2050%25%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20all%200.3s%20ease%3B%0A%20%20%20%20%20%20transition%3A%20all%200.3s%20ease%3B%0A%20%20%20%20%20%20%20%20border-radius%3A4px%3B%0A%7D%0A%20%20%20%20%20%20%20%20%20%20%0A%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet-imageoverlay%20%7B%0A%20%20%20%20%20%20%20%20z-index%3A2%3B%0A%20%20%20%20%20%20%20%20-webkit-transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20%20transform%3AtranslateX%28-50%25%29%3B%0A%20%20%20%20%20%20%20-webkit-transform%3AtranslateX%28-50%25%29%3B%0A%7D%20%20%20%20%20%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet-title%20%7B%20%0A%20%20%20%20%20%20%20%20color%3A%23ffffff%3B%0A%20%20%20%20%20%20%20%20text-align%3Acenter%3B%0A%20%20%20%20%20%20%20%20line-height%3A15px%3B%0A%20%20%20%20%20%20%20%20font-size%3A13px%3B%0A%20%20%20%20%20%20%20%20font-weight%3A600%3B%20%20%0A%20%20%20%20%20%20%20%20z-index%3A3%3B%0A%20%20%20%20%20%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20%20%20opacity%3A0%3B%0A%20%20%20%20%20%20%20%20%20-webkit-backface-visibility%3A%20hidden%3B%20%0A%20%20%20%20%20%20%20%20backface-visibility%3A%20hidden%3B%0A%20%20%20%20%20%20%20%20-webkit-transform-origin%3A%2050%25%2050%25%3B%0A%20%20%20%20transform-origin%3A%2050%25%2050%25%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20all%200.3s%20ease%3B%0A%20%20%20%20%20%20transition%3A%20all%200.3s%20ease%3B%0A%20%20%20%20%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20%20%20%20%20bottom%3A45px%3B%0A%20%20%20%20%20%20%20%20width%3A135px%3B%0A%20%20%20%20%20%20vertical-align%3Amiddle%3B%0A%20%20%20%20%20%20%20%0A%7D%0A%20%20%20%20%20%20%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%3Ahover%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%3Ahover%20.tp-bullet-image%2C%0A%23rev_slider_1_1_wrapper%20.zeus%20.tp-bullet%3Ahover%20.tp-bullet-imageoverlay%7B%0A%20%20%20%20%20%20opacity%3A1%3B%0A%20%20%20%20%20%20visibility%3Avisible%3B%0A%20%20%20%20-webkit-transform%3AtranslateY%280px%29%20translateX%28-50%25%29%3B%0A%20%20%20%20%20%20transform%3AtranslateY%280px%29%20translateX%28-50%25%29%3B%20%20%20%20%20%20%20%20%20%0A%20%20%20%20%7D%0A%0A%0A%0A%0A%2F%2A%20VERTICAL%20RIGHT%20%2A%2F%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet-image%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet-imageoverlay%7B%0A%20%20bottom%3Aauto%3B%0A%20%20margin-right%3A10px%3B%0A%20%20margin-bottom%3A0px%3B%0A%20%20right%3A13px%3B%0A%20%20transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet%3Ahover%20.tp-bullet-image%20%7B%0A%20%20transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%7D%0A%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet-imageoverlay%20%7B%0A%20%20%20%20%20%20%20%20z-index%3A2%3B%0A%20%20%20%20%20%20%20%20-webkit-transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20%20transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20%20%20%20%20%20-webkit-transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%7D%20%20%20%0A%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet-title%20%7B%0A%20%20%20%20%20bottom%3Aauto%3B%0A%20%20%20%20%20right%3A100%25%3B%0A%20%20%20%20%20margin-right%3A10px%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet%3Ahover%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet%3Ahover%20.tp-bullet-image%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical%20.tp-bullet%3Ahover%20.tp-bullet-imageoverlay%20%7B%0A%20transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%7D%0A%0A%0A%0A%2F%2A%20VERTICAL%20LEFT%20%2A%2F%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-image%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-imageoverlay%7B%0A%20%20bottom%3Aauto%3B%0A%20%20margin-left%3A10px%3B%0A%20%20margin-bottom%3A0px%3B%0A%20%20left%3A13px%3B%0A%20%20transform%3A%20%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet%3Ahover%20.tp-bullet-image%20%7B%0A%20%20transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20translateX%280px%29%20translateY%28-50%25%29%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-imageoverlay%20%7B%0A%20%20%20%20%20%20%20%20z-index%3A2%3B%0A%20%20%20%20%20%20%20%20-webkit-transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20%20transform%3AtranslateX%280px%29%20translateY%28-50%25%29%3B%0A%20%20%20%20%20%20%20-webkit-transform%3AtranslateX%280px%29%20translateY%28-50%25%29%3B%0A%7D%20%20%20%0A%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-vertical.nav-pos-hor-left%20.tp-bullet-title%20%7B%0A%20%20%20%20%20bottom%3Aauto%3B%0A%20%20%20%20%20left%3A100%25%3B%0A%20%20%20%20%20margin-left%3A10px%3B%0A%7D%0A%0A%2F%2A%20HORIZONTAL%20TOP%20%2A%2F%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-image%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-imageoverlay%7B%0A%20%20bottom%3Aauto%3B%0A%20%20top%3A13px%3B%0A%20%20margin-top%3A10px%3B%0A%20%20margin-bottom%3A0px%3B%0A%20%20left%3A0px%3B%0A%20%20transform%3AtranslateY%280px%29%20translateX%28-50%25%29%3B%0A%20%20-webkit-transform%3AtranslateX%280px%29%20translateX%28-50%25%29%3B%0A%20%20%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet%3Ahover%20.tp-bullet-image%20%7B%0A%20%20%0A%20%20transform%3A%20scale%281%29%20translateY%280px%29%20translateX%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20scale%281%29%20translateY%280px%29%20translateX%28-50%25%29%3B%0A%20%20%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%2C%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-imageoverlay%20%7B%0A%20%20%20%20%20%20%20%20z-index%3A2%3B%0A%20%20%20%20%20%20%20%20-webkit-transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20transition%3A%20all%200.5s%20ease%3B%0A%20%20%20%20%20%20%20%20transform%3AtranslateY%280px%29%20translateX%28-50%25%29%3B%0A%20%20%20%20%20%20%20-webkit-transform%3AtranslateY%280px%29%20translateX%28-50%25%29%3B%0A%7D%20%20%20%0A%0A%0A%23rev_slider_1_1_wrapper%20.zeus.nav-dir-horizontal.nav-pos-ver-top%20.tp-bullet-title%20%7B%0A%20%20%20%20%20bottom%3Aauto%3B%0A%20%20%20%20%20top%3A13px%3B%0A%20%20%20%20%20margin-top%3A20px%3B%0A%7D%0A%0A");
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				</script>
				<script>
					var htmlDivCss = unescape("%0A%0A");
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				</script>
			</rs-module-wrap>
			<!-- END REVOLUTION SLIDER -->




</div></div></div></div></div>

  <!-- FIN PORTADA ---->



<!---NOSOTROS AQUI---->

<div class="vc-row-container container"><div id="overview" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1463148379933 vc_row-has-fill row-stretch"><div class="wpb_column vc_column_container vc_col-sm-7 vc_col-lg-7 vc_col-md-6 vc_col-xs-12"><div class="vc_column-inner vc_custom_1498220066719"><div class="wpb_wrapper"><div class="section-title  title">
    <H2>Crea, innova y lanza tu negocio!</H2>
<p>Estudiamos tu idea de emprendimiento!</p>
</div>
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<p>Somos un equipo de trabajo organizados en diferentes areas, enfocados en estrategias de desarrollo web, nos dedicamos a crear proyectos que quieran innovar en la web así como el tuyo. potencializamos tus ideas en una aplicación, en una página, blog o todo lo que se te ocurra para exponer tu emprendimiento. Lo haremos por ti.</p>

		</div>
<br>
		<div class="btn_accion"  ><a  href="#contacto"><span>Empieza ya!</span></a></div>
	
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner vc_custom_1498224592367"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left right-to-left  retina centered devices">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="826" height="436" src="<?= base_url() ?>/public/image/nosotros/dispositivos.png" class="vc_single_image-img attachment-full" alt="All screen sizes" srcset="<?= base_url() ?>/public/image/nosotros/dispositivos.png" sizes="(max-width: 826px) 100vw, 826px" /></div>
		</figure>
	</div>


</div></div></div></div>

<div class="vc_row-full-width vc_clearfix"></div></div>
  <!-- FIN NOSOTROS ---->



  
  <!-- SERVICIOS AQUI ---->

<div class="vc-row-container container">
		<div id="features" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1463148754510 vc_row-has-fill row-stretch">
		        <div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner vc_custom_1498219725582">
						    <div class="wpb_wrapper">
								<div class="section-title  title centered">
									<H2>Lo que ofrecemos</H2>
								<p>Hablemos de tu idea de negocio y lo haremos!</p>
								</div>

								<div class="vc_row wpb_row vc_inner vc_row-fluid features container-fixed">
									<div class="wpb_column vc_column_container vc_col-sm-4">
											<div class="vc_column-inner">
												<div class="wpb_wrapper">
															<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1485528964151">		
																<figure class="wpb_wrapper vc_figure">
																	<div class="vc_single_image-wrapper   vc_box_border_grey">
																	<img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi1.png" class="vc_single_image-img attachment-full" alt="" /></div>
																</figure>
															</div>
															<div class="section-title  feature-block">
																<H2>Páginas web</H2>
															<p>Páginas web con los requisitos básicos pero funcional para  ayudar a crecer tu negocio de forma digital.</p>
															<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

															</div>
				
												</div>
											</div>
									</div>




									<div class="wpb_column vc_column_container vc_col-sm-4">
								        	<div class="vc_column-inner">
									               <div class="wpb_wrapper">
															<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261818858">
																
																<figure class="wpb_wrapper vc_figure">
																	<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi2.png" class="vc_single_image-img attachment-full" alt="" /></div>
																</figure>
															</div>
															<div class="section-title  feature-block">
																<H2>Aplicaciones web</H2>
															<p>Desarrollo con estanderes a la vanguardia, se adapta a la necesidad del cliente, dinámicas y adaptable a tu dispositivo-responsive</p>
															<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

															</div>

                                                    </div>
                                            </div>
                                    </div>



								     <div class="wpb_column vc_column_container vc_col-sm-4">
								        	 <div class="vc_column-inner">
								                	<div class="wpb_wrapper">
																<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261826530">
																	
																	<figure class="wpb_wrapper vc_figure">
																		<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi3.png" class="vc_single_image-img attachment-full" alt="" /></div>
																	</figure>
																</div>
																<div class="section-title  feature-block">
																	<H2>Aplicaciones predeterminadas</H2>
																    <p>Instalamos en tu proyecto web nuestras aplicaciones predeterminadas  y podrás hacer uso de esta cuando lo requieras.</p>
																	<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

																</div>

                                                    </div>
                                             </div>
                                    </div>
                                 </div>

								<div class="vc_row wpb_row vc_inner vc_row-fluid features container-fixed">
								    <div class="wpb_column vc_column_container vc_col-sm-4">
								            <div class="vc_column-inner">
								                    <div class="wpb_wrapper">
																	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261833695">
																		
																		<figure class="wpb_wrapper vc_figure">
																			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi4.png" class="vc_single_image-img attachment-full" alt="" /></div>
																		</figure>
																	</div>
																	<div class="section-title  feature-block">
																		<H2>Instalación de hosting, VPS, dominios</H2>
																	<p>Instalamos tu proyecto web, con todo los requirimientos que esta contenga.</p>
																	<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

																	</div>
													</div>
											</div>
									 </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                            <div class="vc_column-inner">
											        <div class="wpb_wrapper">
	                                                                <div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261844757">
		
																		<figure class="wpb_wrapper vc_figure">
																			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi5.png" class="vc_single_image-img attachment-full" alt="" /></div>
																		</figure>
																	</div>
																	<div class="section-title  feature-block">
																		<H2>Migraciones de APP, VPS, BD</H2>
																	<p>Migramos tus aplicaciones y tús bases de datos  a otros servidores, hosting o VPS</p>
																	<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

																	</div>
													</div>
											</div>
										</div>

										<div class="wpb_column vc_column_container vc_col-sm-4">
										    <div class="vc_column-inner">
													<div class="wpb_wrapper">
																	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261852421">
																		
																		<figure class="wpb_wrapper vc_figure">
																			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi6.png" class="vc_single_image-img attachment-full" alt="" /></div>
																		</figure>
																	</div>
																	<div class="section-title  feature-block">
																		<H2>Diseño de Base de datos</H2>
																	<p>Diseñamos y modelamos la lógica de tu proyecto web, creamos tus bases de datos.</p>
																	<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

																	</div>
													</div>
											</div>
										</div>

								</div>


						
<div class="vc_row wpb_row vc_inner vc_row-fluid features container-fixed">
								   
<div class="wpb_column vc_column_container vc_col-sm-4">
   <div class="vc_column-inner">
       <div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261852421">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi7.png" class="vc_single_image-img attachment-full" alt="" /></div>
				</figure>
			</div>
			<div class="section-title  feature-block">
				<H2>SEO Básico</H2>
			<p>Optimizamos  tu proyecto web en herramientas de SEO, para que puedas posicionarte  en los motores de búsqueda de la web.</p>
			<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

			</div>
		</div>
	</div>
</div>

<div class="wpb_column vc_column_container vc_col-sm-4">
   <div class="vc_column-inner">
        <div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261852421">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi8.png" class="vc_single_image-img attachment-full" alt="" /></div>
				</figure>
			</div>
			<div class="section-title  feature-block">
				<H2>INTEGRACIONES API'S</H2>
			<p>Integramos las funcionalidades de otras aplicaciones web con tu proyecto y puedas agilizar procesos de las funciones de tu sitio.</p>
			<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

			</div>
		</div>
	</div>
</div>

<div class="wpb_column vc_column_container vc_col-sm-4">
   <div class="vc_column-inner">
       <div class="wpb_wrapper">
			<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1464261852421">
				
				<figure class="wpb_wrapper vc_figure">
					<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="128" height="128" src="<?= base_url() ?>/public/image/servicios/servi9.png" class="vc_single_image-img attachment-full" alt="" /></div>
				</figure>
			</div>
			<div class="section-title  feature-block">
				<H2>CERTIFICADO SSL</H2>
			<p>Tu proyectó web necesita seguridad, es necesario tener certificados SSL y navegar correctamente en modo seguro.</p>
			<div class="btn_accion2"  ><a  href="#contacto"><span>Adquirir servicio!</span></a></div>

			</div>
		</div>
	</div>

</div>

</div>
</div></div></div></div>


<div class="vc_row-full-width vc_clearfix"></div></div>


<!--PORTAFOLIO AQUI----->
<div class="vc-row-container container">
<div id="portafolio" class="vc_row wpb_row vc_row-fluid">
<div class="wpb_column vc_column_container vc_col-sm-12">
<div class="vc_column-inner"><div class="wpb_wrapper">
			<!-- START Apple Watch REVOLUTION SLIDER 6.2.12 --><p class="rs-p-wp-fix"></p>
			<rs-module-wrap id="rev_slider_2_2_wrapper" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
				<rs-module id="rev_slider_2_2" style="" data-version="6.2.12">
					<rs-slides>
						<rs-slide data-key="rs-2" data-title="Slide" data-anim="ei:d;eo:d;s:600;r:0;t:fade;sl:d;">
							<img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/sky.png" title="Homepage" data-bg="p:left top;f:contain;r:repeat-x;" class="rev-slidebg" data-no-retina>
<!--
							--><rs-layer
								id="slider-2-slide-2-layer-6" 
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:532px;y:156px;"
								data-text="l:22;"
								data-dim="w:570px;h:163px;"
								data-frame_0="o:1;tp:600;"
								data-frame_1="tp:600;e:none;st:500;sp:0;sR:500;"
								data-frame_999="x:-175%;o:0;tp:600;e:none;st:w;sp:0;sR:8500;"
								data-loop_999="x:-300;sp:30000;yym:t;yys:t;yyf:t;"
								style="z-index:5;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/clouds.png" width="1140" height="325" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-2-slide-2-layer-1" 
								data-type="text"
								data-color="rgba(44, 49, 66, 1)"
								data-rsp_ch="on"
								data-xy="x:40px;y:183px;"
								data-text="s:52;l:22;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:6;"
							>Poryectos significativos! 
							</rs-layer><!--

							--><rs-layer
								id="slider-2-slide-2-layer-2" 
								data-type="text"
								data-color="rgba(114, 121, 144, 1)"
								data-rsp_ch="on"
								data-xy="x:40px;y:244px;"
								data-text="s:25;l:22;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:7;"
							>Queremos ser parte de tu proyecto web, sumate!  
							</rs-layer><!--

							--><rs-layer
								id="slider-2-slide-2-layer-3" 
								data-type="text"
								data-color="rgba(133, 136, 145, 1)"
								data-rsp_ch="on"
								data-xy="x:40px;y:301px;"
								data-text="w:normal;s:21;l:30;"
								data-dim="w:514px;h:307px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:8;"
							>Hemos dedicado más de 8 años al desarrollo de plataformas web, nos interesa que el proyecto finalice con éxito y por eso dedicamos el tiempo necesario para mantener una comunicación acertiva y eficiente contigo.
 
							</rs-layer><!--

							--><rs-layer
								id="slider-2-slide-2-layer-4" 
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:660px;y:92px;"
								data-text="l:22;"
								data-dim="w:470px;"
								data-frame_0="y:-50px;tp:600;"
								data-frame_1="tp:600;e:back.out;st:100;sp:800;sR:100;"
								data-frame_999="tp:600;e:back.in;st:w;sp:800;sR:8100;"
								style="z-index:9;"
							><img src="<?= base_url() ?>/public/image/portafolio/principal.png" data-no-retina> 
							</rs-layer><!--

							--><rs-layer
								id="slider-2-slide-2-layer-5" 
								data-type="image"
								data-rsp_ch="on"
								data-xy="x:753px;y:360px;"
								data-text="l:22;"
								data-dim="w:342px;h:132px;"
								data-frame_0="tp:600;"
								data-frame_1="tp:600;e:power2.inOut;st:500;sR:500;"
								data-frame_999="o:0;tp:600;e:nothing;st:w;sR:8200;"
								style="z-index:10;"
							><img src="https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/uploads/2016/05/watch-shadow-1.png" width="685" height="132" data-no-retina> 
							</rs-layer><!--
-->						</rs-slide>
					</rs-slides>
				</rs-module>
				<script type="text/javascript">
					setREVStartSize({c: 'rev_slider_2_2',rl:[1240,1024,768,480],el:[],gw:[1170],gh:[710],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
					var	revapi2,
						tpj;
					jQuery(function() {
						tpj = jQuery;
						revapi2 = tpj("#rev_slider_2_2")
						if(revapi2==undefined || revapi2.revolution == undefined){
							revslider_showDoubleJqueryError("rev_slider_2_2");
						}else{
							revapi2.revolution({
								sliderLayout:"fullwidth",
								visibilityLevels:"1240,1024,768,480",
								gridwidth:1170,
								gridheight:710,
								spinner:"spinner0",
								perspectiveType:"local",
								responsiveLevels:"1240,1024,768,480",
								progressBar:{disableProgressBar:true},
								navigation: {
									onHoverStop:false
								},
								viewPort: {
									enable:true,
									visible_area:"20%"
								},
								fallbacks: {
									allowHTML5AutoPlayOnAndroid:true
								},
							});
						}
						
					});
				</script>
			</rs-module-wrap>
			<!-- END REVOLUTION SLIDER -->
<div class="section-title  title centered vc_custom_1463055785974">
    <H2>Alguno de nuestros proyectos culminados.</H2>
<p>Clientes satisfechos y desarrollo completo<br />
Esperamos verte en nuestro portafolio pronto.</p>
</div>
<div class="vc_row wpb_row vc_inner vc_row-fluid features vc_custom_1464268545885 container-fixed">

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
											<div  class="wpb_single_image wpb_content_element vc_align_center">
												
												<figure class="wpb_wrapper vc_figure" id='corte_img'>
												<h3 class="titulos">Plataforma fintech  Crowdfunding</h3>
												<div class="btn_accion3"  ><a href="https://cofinancer.co/" target="_blank">CoFinancer SAS
													<div class="vc_single_image-wrapper   vc_box_border_grey"><img   src="<?= base_url() ?>/public/image/portafolio/portafolio1.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
													</a>
												</div>
												</figure>
											</div>
									</div>
									</div>
				</div>

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">Página web dinámica - cotizador</h3>
													<div class="btn_accion3"  ><a href="http://ematuecob.com.co/" target="_blank">Ematuecob SAS
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio2.png" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>

				</div>



               <div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">API cotizador de envíos</h3>
													<div class="btn_accion3"  ><a href="https://enviosinternacionales.co/" target="_blank">Envios internacionales
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio3.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>
               </div>
</div>



<div class="vc_row wpb_row vc_inner vc_row-fluid features vc_custom_1464268545885 container-fixed">

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
											<div  class="wpb_single_image wpb_content_element vc_align_center">
												
												<figure class="wpb_wrapper vc_figure" id='corte_img'>
												<h3 class="titulos">Página de presentación</h3>
												<div class="btn_accion3"  ><a href="http://hgjosuedavid.com/" target="_blank">Realizador HG Josue David
													<div class="vc_single_image-wrapper   vc_box_border_grey"><img   src="<?= base_url() ?>/public/image/portafolio/portafolio4.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
													</a>
												</div>
												</figure>
											</div>
									</div>
									</div>
				</div>

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">Migración a servidor VPS </h3>
													<div class="btn_accion3"  ><a href="http://comfenalcoeps.com/" target="_blank">Comfenalco EPS
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio5.png" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>

				</div>



               <div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">Colaboración app administrativo</h3>
													<div class="btn_accion3"  ><a href="http://intergrafic.com.co/" target="_blank">Intregrafic de Occidente SA
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio6.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>
               </div>
</div>




<div class="vc_row wpb_row vc_inner vc_row-fluid features vc_custom_1464268545885 container-fixed">

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
											<div  class="wpb_single_image wpb_content_element vc_align_center">
												
												<figure class="wpb_wrapper vc_figure" id='corte_img'>
												<h3 class="titulos">Colaboración tienda virtual</h3>
												<div class="btn_accion3"  ><a href="https://exportala.com/" target="_blank">Exportala.com
													<div class="vc_single_image-wrapper   vc_box_border_grey"><img   src="<?= base_url() ?>/public/image/portafolio/portafolio7.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
													</a>
												</div>
												</figure>
											</div>
									</div>
									</div>
				</div>

				<div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">Configuración plantilla web</h3>
													<div class="btn_accion3"  ><a href="http://mipres.app/" target="_blank">Mipress.app
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio8.png" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>

				</div>



               <div class="wpb_column vc_column_container vc_col-sm-4">
									<div class="vc_column-inner">
									<div class="wpb_wrapper">
												<div  class="wpb_single_image wpb_content_element vc_align_center">
													
													<figure class="wpb_wrapper vc_figure" id='corte_img'>
													<h3 class="titulos">Instalación plantilla web</h3>
													<div class="btn_accion3"  ><a href="http://marxmarquinez.com/" target="_blank">Marxmarquinez.com
														<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="319" height="577" src="<?= base_url() ?>/public/image/portafolio/portafolio9.png" class="vc_single_image-img attachment-full" alt="" sizes="(max-width: 319px) 100vw, 319px" /></div>
														</a>
													</div>
													</figure>
												</div>
									</div>
									</div>
               </div>
</div>




</div>


</div></div></div></div>

<!--FIN PORTAFOLIO----->


<!--OFERTA AQUI----->

<div class="vc-row-container container parent--weather-icons"><div id="ofertas" class="vc_row wpb_row vc_row-fluid weather-icons vc_custom_1464268532742"><div class="wpb_column vc_column_container vc_col-sm-5"><div class="vc_column-inner"><div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1463053160569">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="959" height="1311" src="<?= base_url() ?>/public/image/oferta.png" class="vc_single_image-img attachment-full" alt=""  sizes="(max-width: 959px) 100vw, 959px" /></div>
		</figure>
	</div>
</div></div></div>
         <div class="unique-icons-section wpb_column vc_column_container vc_col-sm-7"><div class="vc_column-inner"><div class="wpb_wrapper">
				<div class="wpb_text_column wpb_content_element  vc_custom_1463052357364 post-formatting " >
					<div class="wpb_wrapper">
						<h2>Oferta especial</h2>
			               <p>20% de descuento, panel admin sistema de usuarios!</p>

					</div>
				</div>
				<div class="vc_row wpb_row vc_inner vc_row-fluid unique-icons-section-holder vc_custom_1498224749148 container-fixed"><div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6"><div class="vc_column-inner vc_custom_1498224614541"><div class="wpb_wrapper">
					<div class="wpb_text_column wpb_content_element  vc_custom_1464261676761 post-formatting " >
						<div class="wpb_wrapper">
							<h3><img class="alignleft size-full wp-image-83" src="<?= base_url() ?>/public/image/icono.png" alt="weather-1" width="98" height="98" />LOG-IN - REGISTRO</h3>

						</div>
					</div>

				<div class="wpb_text_column wpb_content_element  vc_custom_1464261716529 post-formatting " >
					<div class="wpb_wrapper">
						<h3><img class="alignleft size-full wp-image-82" src="<?= base_url() ?>/public/image/icono.png" alt="weather-2" width="98" height="98" />PANEL  PERFIL</h3>

					</div>
				</div>

				<div class="wpb_text_column wpb_content_element  vc_custom_1464261725872 post-formatting " >
					<div class="wpb_wrapper">
						<h3><img class="alignleft size-full wp-image-81" src="<?= base_url() ?>/public/image/icono.png" alt="weather-3" width="98" height="98" />PANEL CRUD </h3>

					</div>
				</div>
	    </div>

	</div>

</div>




<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-6"><div class="vc_column-inner vc_custom_1498224617960"><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1464261732658 post-formatting " >
		<div class="wpb_wrapper">
			<h3><img class="alignleft size-full wp-image-80" src="<?= base_url() ?>/public/image/icono.png" alt="weather-4" width="98" height="98" />PANEL ROLES</h3>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1464261739831 post-formatting " >
		<div class="wpb_wrapper">
			<h3><img class="alignleft size-full wp-image-79" src="<?= base_url() ?>/public/image/icono.png" alt="weather-5" width="98" height="98" />PANEL  DE TAREAS</h3>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  vc_custom_1464261747536 post-formatting " >
		<div class="wpb_wrapper">
			<h3><img class="alignleft size-full wp-image-78" src="<?= base_url() ?>/public/image/icono.png" alt="weather-6" width="98" height="98" />PANEL ANALÍTICA</h3>

		</div>
	</div>
</div></div></div></div></div></div></div></div></div>


<!--FIN OFERTA----->


<!--CONTACTO AQUI----->

<div id="contacto" class="embed-container">
<iframe id="form_contacto"  src="<?= base_url() ?>/public/assets/formulario_contacto/index.php" frameborder="0" allowfullscreen></iframe>
</div>


<div class="vc-row-container container" ><div style="background-color:#fbe4af" id="contacto" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid  vc_row-has-fill row-stretch"><div class="phones wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner vc_custom_1498219675912">
<div class="wpb_wrapper">
	<div  class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1463059209186  retina">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="845" height="969" src="<?= base_url() ?>/public/image/trabajo.png" class="vc_single_image-img attachment-full" alt=""  sizes="(max-width: 845px) 100vw, 845px" /></div>
		</figure>
	</div>
</div></div></div><div class="app-store wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner vc_custom_1498219678994">
<div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<h2><span style="color: #ffffff;">Seguimos trabajando,</span><br />
<span style="color: #ffffff;"> Deseas programar una <strong>Reunión</strong> ?</span></h2>

		</div>
	</div>

	<div class="wpb_text_column wpb_content_element  post-formatting " >
		<div class="wpb_wrapper">
			<p><a href="https://meet.google.com/" target="_blank"><img class="alignleft size-medium wp-image-133" src="<?= base_url() ?>/public/image/meet.png" alt="appstore" width="200" height="85" sizes="(max-width: 300px) 100vw, 300px" /></a></p>

		</div>
	</div>
</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div></div>
    </div>
</div><!-- .wrapper -->






<footer id="footer" style='background-color:#ff9201' role="contentinfo" class="site-footer main-footer footer-bottom-horizontal fixed-footer site-footer-inverted main-footer-inverted">

	
	
        <div class="footer-bottom">

            <div class="container">

                <div class="footer-bottom-content">

					
                        <div class="footer-content-right">
							<ul class="social-networks rounded colored-hover"><li><a href="https://www.facebook.com/Wudbi-104990764614165/" target="_blank" class="facebook" title="Facebook" aria-label="Facebook" rel="noopener"><i class="fa fa-facebook"></i><span class="name">Facebook</span></a></li>
							<!--<li><a href="https://twitter.com/thelaborator" target="_blank" class="twitter" title="Twitter" aria-label="Twitter" rel="noopener"><i class="fa fa-twitter"></i><span class="name">Twitter</span></a></li>-->
							<li><a href="https://www.instagram.com/wudbiweb/" target="_blank" class="instagram" title="Instagram" aria-label="Instagram" rel="noopener"><i class="fa fa-instagram"></i><span class="name">Instagram</span></a></li>
							<li><a href="http://api.whatsapp.com/send?phone=573012698569" target="_blank" class="whatsapp" title="WhatsApp" aria-label="WhatsApp" rel="noopener"><i class="fa fa-whatsapp"></i><span class="name">WhatsApp</span></a></li>
							<li><a href="https://www.youtube.com/channel/UCpVA4ucD0ZBpqXQ3kth1Ttg?view_as=public" target="_blank" class="youtube" title="YouTube" aria-label="YouTube" rel="noopener"><i class="fa fa-youtube-play"></i><span class="name">YouTube</span></a></li>
							<!--<li><a href="" target="_blank" class="twitch" title="Twitch" aria-label="Twitch" rel="noopener"><i class="fa fa-twitch"></i><span class="name">Twitch</span></a></li>-->
							</ul>                        </div>

					
					
                        <div class="footer-content-left">
                                            <img src="<?= base_url() ?>/public/image/logo/logo_blanco.png" width="150"/>
                            <div class="copyrights site-info">
                                <p>©  <?php echo date("Y")?> <a href="<?= base_url() ?>" target="_blank" rel="noopener">Plataformas web</a> Creadas por <a href="http://wudbi.com" target="_blank" rel="noopener">wudbi.com</a></p>
                            </div>

                        </div>

					                </div>

            </div>

        </div>

	
</footer><script type="application/ld+json">{"@context":"https:\/\/schema.org\/","@type":"Organization","name":"App Landing","url":"https:\/\/demo.kaliumtheme.com\/landing","logo":"https:\/\/demo.kaliumtheme.com\/landing\/wp-content\/uploads\/2016\/05\/logo.png"}</script>    <a href="#top" class="go-to-top position-bottom-right rounded" data-type="pixels" data-val="0">
        <i class="flaticon-bottom4"></i>
    </a>
	<script type="text/html" id="wpb-modifications"></script>		<script type="text/javascript">
		if(typeof revslider_showDoubleJqueryError === "undefined") {
			function revslider_showDoubleJqueryError(sliderID) {
				var err = "<div class='rs_error_message_box'>";
				err += "<div class='rs_error_message_oops'>Oops...</div>";
				err += "<div class='rs_error_message_content'>";
				err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
				err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
				err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
				err += "</div>";
			err += "</div>";
				var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
			}
		}
		</script>
		<script type="text/javascript">
		if(typeof revslider_showDoubleJqueryError === "undefined") {
			function revslider_showDoubleJqueryError(sliderID) {
				var err = "<div class='rs_error_message_box'>";
				err += "<div class='rs_error_message_oops'>Oops...</div>";
				err += "<div class='rs_error_message_content'>";
				err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
				err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
				err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
				err += "</div>";
			err += "</div>";
				var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
			}
		}
		</script>
<link rel='stylesheet' id='vc_animate-css-css'  href='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css?ver=6.2.0' media='all' />
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/libs/gsap/gsap.min.js?ver=3.0.5.001'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/libs/gsap/ScrollToPlugin.min.js?ver=3.0.5.001'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/libs/scrollmagic/ScrollMagic.min.js?ver=3.0.5.001'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/libs/scrollmagic/plugins/animation.gsap.min.js?ver=3.0.5.001'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/js/sticky-header.min.js?ver=3.0.5.001'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-includes/js/wp-embed.min.js?ver=5.4.2'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=6.2.0'></script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min.js?ver=6.2.0'></script>
<script type='text/javascript'>
var _k = _k || {}; _k.stickyHeaderOptions = {"type":"standard",
"containerElement":".site-header","logoElement":".logo-image","triggerOffset":0,
"offset":".top-header-bar","animationOffset":10,"spacer":false,"animateProgressWithScroll":true,
"animateDuration":null,"tweenChanges":false,"classes":{"name":"site-header","prefix":"sticky",
"init":"initialized","fixed":"fixed","absolute":"absolute","spacer":"spacer","active":"active",
"fullyActive":"fully-active"},"autohide":{"animationType":"fade-slide-top",
"duration":0.299999999999999988897769753748434595763683319091796875,"threshold":100},
"animateScenes":{"styling":{"name":"style","selector":".header-block",
"props":["backgroundColor","boxShadow"],"css":{"default":{"backgroundColor":"#ffffff",
"boxShadow":"rgba(0,0,0,0.10) 0px 0px 40px"}},"data":{"tags":["transparent-header"]},
"position":0},"sticky-logo":{"name":"sticky-logo","selector":"logo","props":["width","height"],
"css":{"width":33,"height":30},"data":{"type":"alternate-logo","alternateLogo":"sticky",
"tags":["logo-switch"]},"position":0},"padding":{"name":"padding","selector":".header-block",
"props":["paddingTop","paddingBottom"],"css":{"default":{"paddingTop":8,"paddingBottom":8}},
"position":0}},"alternateLogos":{"sticky":{"name":"sticky",
"image":"<img width=\"200\" height=\"0\" src=\"http:\/\/wudbi.com\/image\/logo\/icono_n.png\" class=\"attachment-original size-original\" alt=\"Wudbi\" srcset=\"http:\/\/wudbi.com\/image\/logo\/icono_n.png 318w, http:\/\/wudbi.com\/image\/logo\/icono_n.png 300w\" sizes=\"(max-width: 318px) 100vw, 318px\" \/>"}},"supportedOn":{"desktop":1,"tablet":1,"mobile":1},"other":{"menuSkin":"menu-skin-main"},"debugMode":false};

var _k = _k || {}; _k.logoSwitchOnSections = [];
var _k = _k || {}; _k.enqueueAssets = {"js":{"light-gallery":[{"src":"https:\/\/demo.kaliumtheme.com\/landing\/wp-content\/themes\/kalium\/assets\/libs\/light-gallery\/lightgallery-all.min.js"}]},"css":{"light-gallery":[{"src":"https:\/\/demo.kaliumtheme.com\/landing\/wp-content\/themes\/kalium\/assets\/libs\/light-gallery\/css\/lightgallery.min.css"},{"src":"https:\/\/demo.kaliumtheme.com\/landing\/wp-content\/themes\/kalium\/assets\/libs\/light-gallery\/css\/lg-transitions.min.css"}]}};
var _k = _k || {}; _k.require = function(e){var t=e instanceof Array?e:[e];return new Promise(function(e,r){var a=function(t){if(t&&t.length){var r=t.shift(),n=r.match( /\.js(\?.*)?$/)?"script":"text";jQuery.ajax({dataType:n,url:r}).success(function(){!function(e){var t;e.match( /\.js(\?.*)?$/)?(t=document.createElement("script")).src=e:((t=document.createElement("link")).rel="stylesheet",t.href=e);var r=!1,a=jQuery("[data-deploader]").each(function(t,a){e!=jQuery(a).attr("src")&&e!=jQuery(a).attr("href")||(r=!0)}).length;r||(t.setAttribute("data-deploader",a),jQuery("head").append(t))}(r)}).always(function(){r.length&&a(t)})}else e()};a(t)})};
</script>
<script type='text/javascript' src='https://demokaliumsites-laborator.netdna-ssl.com/landing/wp-content/themes/kalium/assets/js/main.min.js?ver=3.0.5.001'></script>
    <script>
		jQuery( document ).ready( function ( $ ) {
			var $ = jQuery;

			$( '.menu-item a' ).each( function () {
				var $el = $( this ),
					text = $el.text().trim().toLowerCase();

				if ( - 1 !== $.inArray( text, ['buy', 'buy!', 'buy now', 'buy now!', 'purchase'] ) ) {
					$el.on( 'click', function ( ev ) {
						fbq( 'track', 'AddToCart', {
							value: 60,
							currency: 'USD',
							contents: [
								{
									id: '10860525_23',
									quantity: 1,
									item_price: 60,
								},
							],
							content_type: 'product',
						} );
					} );
				}
			} );
		} );
    </script>
	
<!-- TET: 0.144982 / 3.0.5ch -->
</body>
</html>