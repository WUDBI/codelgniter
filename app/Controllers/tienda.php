<?php namespace App\Controllers;

class Tienda extends BaseController
{
	public function index()
	{
		return view('tienda/index');
	}
	
	public function config_servicio()
	{
		return view('tienda/config_servicio');
	}
	
	public function config_categoria()
	{
		return view('tienda/config_categoria');
	}
	
	public function config_clasificacion()
	{
		return view('tienda/config_clasificacion');
	}
	public function config_item()
	{
		return view('tienda/config_item');
	}
	
	public function config_redes()
	{
	return view('tienda/config_redes');
	}

	public function config_informe()
	{
	return view('tienda/config_informe');
	}

	//--------------------------------------------------------------------

}
