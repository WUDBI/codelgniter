<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Contacto</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">

		<!-- MATERIAL DESIGN ICONIC FONT -->
		<link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">

		<!-- STYLE CSS -->
		<link rel="stylesheet" href="css/style.css">
	</head>
	<body>
		<div class="wrapper" style="padding:50px">
            <form action="" id="wizard">
        		<!-- SECTION 1 -->
                <h2></h2>
                <section>
                    <div class="inner">
						<div class="image-holder">
							<img src="images/img1.jpg" alt="">
						</div>
						<div class="form-content" >
							<div class="form-header">
								<h3>Registrate y contáctanos</h3>
							</div>
							<p>Por favor llena los campos requeridos</p>
							<div class="form-row">
								<div class="form-holder">
									<input type="text" required name="user_nombre" placeholder="Nombre completo" class="form-control">
								</div>
								<div class="form-holder">
									<input type="text" required name="user_apellido" placeholder="Apellidos" class="form-control">
								</div>
							</div>
							<div class="form-row">
								<div class="form-holder">
									<input type="text" required name="user_email" placeholder="Email" class="form-control">
								</div>
								<div class="form-holder">
									<input type="text" required name="user_telefono" placeholder="Telefono" class="form-control">
								</div>
							</div>
							<div class="form-row">
								
								<div class="form-holder" style="align-self: flex-end; transform: translateY(4px);">
									<div class="checkbox-tick">
										<h4>¿Con quién deseas contactar?</h4>
										<label class="male">
											<input type="radio" name="user_area" value="web" checked> Área de ventas<br>
											<span class="checkmark"></span>
										</label>
										<label class="female">
											<input type="radio" name="user_area" value="app"> Área de soporte<br>
											<span class="checkmark"></span>
										</label>

										
									</div>
								</div>
							</div>
							<div class="checkbox-circle">
								<label>
									<input type="checkbox" name="user_newletter" checked> Deseas que te enviemos nuestras ofertas por email?
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
                </section>

				<!-- SECTION 2 -->
                <h2></h2>
                <section>
                    <div class="inner">
						<div class="image-holder">
							<img src="images/img2.jpg" alt="">
						</div>
						<div class="form-content">
							<div class="form-header">
								<h3>Registrate y contáctanos</h3>
							</div>
							<p>Ingresa esta información adicional</p>
							<div class="form-row">
								<div class="form-holder w-100">
									<input type="text"  name="user_direccion" placeholder="Dirección" class="form-control">
								</div>
							</div>
							<div class="form-row">
								<div class="form-holder">
									<input type="text" name="user_ciudad" placeholder="Ciudad/municipio" class="form-control">
								</div>
							
							</div>

							<div class="form-row">
								<div class="select" name="user_servicio">
									<div class="form-holder">
										<div class="select-control">Qué buscas?</div>
										<i class="zmdi zmdi-caret-down"></i>
									</div>
									<ul class="dropdown">
										<li rel="app">Aplicativo web</li>
										<li rel="pagina">Página web</li>
										<li rel="vps">Instalación de servidores VPS</li>
										<li rel="hosting_dominio">Hosting y dominio</li>
										<li rel="otro">Otro</li>
									</ul>
								</div>
								<div class="form-holder"></div>
							</div>
						</div>
					</div>
                </section>

                <!-- SECTION 3 -->
                <h2></h2>
                <section>
                    <div class="inner">
						<div class="image-holder">
							<img src="images/img3.jpg" alt="">
						</div>
						<div class="form-content">
							<div class="form-header">
								<h3>Registrate y contáctanos</h3>
							</div>
							<p>Este campo es opcional</p>
							<div class="form-row">
								<div class="form-holder w-100">
									<textarea name="" id="" placeholder="Escribenos tu mensaje aquí!" class="form-control" style="height: 99px;"></textarea>
								</div>
							</div>
							<div class="checkbox-circle mt-24">
								<label>
									<input type="checkbox" checked  required>  Por favor indicanos si <a href="#">Aceptas los términos y condiciones ?</a>
									<span class="checkmark"></span>
								</label>
							</div>
						</div>
					</div>
                </section>
            </form>
		</div>

		<!-- JQUERY -->
		<script src="js/jquery-3.3.1.min.js"></script>

		<!-- JQUERY STEP -->
		<script src="js/jquery.steps.js"></script>
		<script src="js/main.js"></script>
		<!-- Template created and distributed by Colorlib -->


</body>
</html>
